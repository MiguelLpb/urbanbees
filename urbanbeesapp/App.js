import React from 'react';
import { Image } from 'react-native';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import LoadingScreen  from './components/screens/LoadingScreen';
import AuthenticationScreen  from './components/screens/AuthenticationScreen';
import LoginScreen from './components/screens/LoginScreen';
import RegisterScreen from './components/screens/RegisterScreen';
import ProfileScreen from './components/screens/ProfileScreen';
import DopmineScreen from './components/screens/DopamineScreen';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import ChallengesScreen from './components/screens/ChallengesScreen';
import ProductsScreen from './components/screens/ProductsScreen';

const authenticationNavegator = createStackNavigator ({  //This navigator is created to decide where will the app go depending if the user is authenticated, 
  AuthenticateUser: AuthenticationScreen,
  Login: LoginScreen,                                    //the variable name "authenticationNavegator" is just the name I gave it
  Register: RegisterScreen                              
});

const RestOfTheApp = createStackNavigator( // This navigator is created for when the User is already autenticated, the screens shown are the rest of the app, starting with Dopamine page
  {
    default: createBottomTabNavigator ( //this is used to create the navigation menu at the bottom of the app 
      {
        Store: {
          screen: ProductsScreen,
          navigationOptions: {
            tabBarIcon: <Image style={{width: 25, height:25}} source={require('./assets/menubuttons/emptycart.png')}></Image>
          }
        },
        Go: {
          screen: DopmineScreen,
          navigationOptions: {
            tabBarIcon: <Image style={{width: 25, height:25}} source={require('./assets/menubuttons/dopaminiconmenu.png')}></Image>//Use inline style to not add style sheet to app.js
          }
        },
        Challenges: {
          screen: ChallengesScreen,
          navigationOptions: {
            tabBarIcon: <Image style={{width: 25, height:25}} source={require('./assets/menubuttons/rocket.png')}></Image>
          }
        },
        Profile: {
          screen: ProfileScreen,
          navigationOptions: {
            tabBarIcon: <Image style={{width: 25, height:25}} source={require('./assets/menubuttons/usericon.png')}></Image>
          }
        },

    },
    {
      defaultNavigationOptions:{
        tabBarOnPress:({navigation, defaultHandler}) =>{
            if(navigation.state.key==='Dopamine'){ 
              navigation.navigate('postModal')
            } else {
              defaultHandler()
            }
        },
      },
      tabBarOptions: {
     activeTintColor:'black',  //When a screen is active, his button will show the name under in the navigation tab
     inactiveTintColor: 'white', //otherwise it will not show anything (the background is also white)
     style: {
      borderTopColor: "black", // the bar has a top border by default, seting it to white makes it desapear
      height:60,
      borderRadius: 8,
      paddingTop: 10,
    }
    },
        initialRouteName: 'Go'  //This code is for the rest of the app to start in the dopamine page
  }
 ),
  postModal: {
    screen: DopmineScreen //if there is an "error" when tapping a button (I dont even know how could that happen but well) the DopaminScreen will open by default
  }
},
{
  mode: 'modal',      //basic settings ( dont matter what they are for)
  headerMode: 'none'    
},
 
  );

export default createAppContainer(
  createSwitchNavigator(
    {
      Loading: LoadingScreen,    //Loading screen
      Authentication:  authenticationNavegator, //Group of screens for authentication
      App: RestOfTheApp  //Group of screens that make the rest of the app
    }
    ,{
      initialRouteName:"Loading",  //First Screen that the app will show
      defaultNavigationOptions: { //this page doesnt need to have the navigation buttons
        header: null
      },   
    },
  )
);