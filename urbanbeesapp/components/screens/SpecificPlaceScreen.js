import React, { useState } from 'react';
import { StyleSheet, Text, View, ImageBackground, ActivityIndicator, Linking, TouchableOpacity, Modal, ScrollView } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { Button } from '../Button';
import { getDistance } from 'geolib';
import * as firebase from 'firebase';
import fire from '../../config';
import stringSelector from '../lenguage/lenguages';
import { Ionicons } from '@expo/vector-icons';
export default class SpecificPlaceScreen extends React.Component {
    static navigationOptions = {
        headerShown: false
    };
    state = {
        placeId: '',
        name: '',
        address: '',
        description: '',
        lat: '',
        lon: '',
        image: 'https://firebasestorage.googleapis.com/v0/b/urbanbees-e9017.appspot.com/o/Atmospheric%20CO2%20in%20years%20-%20Graph.jpg?alt=media&token=c9b0f816-3c2b-4de3-9cc0-4f0a97c342d8',
        link: '',
        urbees: '',
        city: global.cityfortheapp,
        showCongratsModal: false,
        showWarningModal: false,
        distanceFromPlace: 0,
        activeClaimButton: false,
        userlat: 0,
        userlon: 0,
        ready: true,
        distance: 0,
        message: '',
    };
    //Get all the info from this place from firebase
    getPlace = () => {
        fire.database().ref('/Cities/' + this.state.city + '/Tplaces/' + this.state.placeId).once('value', snapshot => {
            this.setState(
                {
                    name: snapshot.child('name').val(),
                    address: snapshot.child('address').val(),
                    description: snapshot.child('description').val(),
                    lat: snapshot.child('lat').val(),
                    lon: snapshot.child('lon').val(),
                    image: snapshot.child('image').val(),
                    link: snapshot.child('link').val(),
                    urbees: snapshot.child('urbees').val(),
                }
            )

        });
    }
    loadLinkInBrowser = () => {
        Linking.openURL(this.state.link).catch(err => console.error("Couldn't load page", err));
    };
    openGps = () => {
        //depending on the platform the url is diferent 
        var scheme = Platform.OS === 'ios' ? 'https://maps.apple.com/?q' : 'https://maps.google.com/?q';
        var url = scheme + this.state.lat + ',' + this.state.lon;
        Linking.openURL(scheme + '=' + this.state.address + '&ll=' + url);
    }
    //Check if the user already claimed Urbees for this place, and if yes dactivate button 
    checkIfuserAlreadyWent = () => {
        var IdofUser = firebase.auth().currentUser.uid;
        firebase.database().ref('/Cities/' + this.state.city + '/Tplaces/' + this.state.placeId + '/Users/').once('value', snapshot => {
            if (snapshot.child(IdofUser).exists) {
                if (!(snapshot.child(IdofUser).child('went').val() == 'yes')) {
                    this.setState({ activeClaimButton: true });
                }
            }
        });
    }

    //Function that with the lat and lon of the place checks if the user is in the same place as this one
    validateUserLocation = () => {
        this.setState({ ready: false });
        let geoOptions = {
            enableHighAccuracy: true,
            timeOut: 20000,
            maximumAge: 60 * 60 * 24
        };
        navigator.geolocation.getCurrentPosition(this.geoSucces, this.geoFailure, geoOptions);
    }
    geoSucces = (position) => {
        this.setState({ userlat: position.coords.latitude });
        this.setState({ userlon: position.coords.longitude });
        var totaldistance = getDistance(
            { latitude: this.state.lat, longitude: this.state.lon },
            { latitude: this.state.userlat, longitude: this.state.userlon }
        );
        this.setState({ distance: totaldistance });
        this.checkUserLocationAndGiveUrbees(this.state.distance);
    }
    geoFailure = (err) => {
        this.setState({ message: stringSelector('apologies_default_text') + err });
        this.setState({ showWarningModal: true });
        this.setState({ ready: true });
    }
    //This Function gives the urbees to the user, sets the level of the ExporerBee and switches the user to already went so he can not collect again
    checkUserLocationAndGiveUrbees = (validatedlocation) => {
        var IdofUser = firebase.auth().currentUser.uid;
        if (validatedlocation < 150) {
            firebase.database().ref('/Cities/' + this.state.city + '/Tplaces/' + this.state.placeId + '/Users/' + IdofUser + '/went').set('yes').then(() => {
                fire.database().ref('/Users/' + IdofUser).once('value', snapshot => {
                    let newUrbees = snapshot.child('urbees').val() + this.state.urbees;
                    snapshot.child('urbees').ref.set(newUrbees);
                    var newturistictimes = 0;
                    //level up the exporerBee
                    if (snapshot.child('turisticttimes').exists) {
                        newturistictimes = snapshot.child('turisticttimes').val() + 1;
                    } else {
                        newturistictimes = 1;
                    }
                    snapshot.child('turisttictimes').ref.set(newturistictimes);
                }).then(() => {
                    this.setState({ message: 'Congrats, you earned ' + this.state.urbees + ' Urbees' });
                    this.setState({ showCongratsModal: true });
                    this.setState({ activeClaimButton: false });
                    this.setState({ ready: true });
                });
            });
        } else {
            this.setState({ message: stringSelector('not_at_distance_title1') + " " + validatedlocation + " " + stringSelector('not_at_distance_title2') + ' ' + stringSelector('not_at_distance_text') });
            this.setState({ showWarningModal: true });
            this.setState({ ready: true });
        }
    }
    componentDidMount() {
        this.setState(
            {
                placeId: this.props.navigation.getParam('placeId', 'no')
            }, () => { // this is for the getPlace function to execute after the setstate is done, otherwise it executes before the place id is set and the database returns null
                this.getPlace();
                this.checkIfuserAlreadyWent();
            });
    }
    render() {
        return (
            <View style={styles.container}>
                <Modal
                    transparent={true}
                    visible={this.state.showWarningModal}
                >
                    <View style={styles.modalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.modal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ showWarningModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.wariningModalText}>
                                {this.state.message}
                            </Text>
                            <Button onPress={() => this.setState({ showWarningModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.showCongratsModal}
                >
                    <View style={styles.modalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.modal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ showCongratsModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.modalText}>
                                {this.state.message}
                            </Text>
                            <Button onPress={() => this.setState({ showCongratsModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <TouchableOpacity style={styles.back} onPress={() => this.props.navigation.goBack()}>
                    <Ionicons name='ios-arrow-back' size={32} color='black'></Ionicons>
                </TouchableOpacity>
                <View style={styles.scrollView}>
                    <ScrollView style={styles.scrollViewScroll}>
                        <View style={styles.containerOfContainers}>
                            <View style={styles.containerTop}>
                                <ImageBackground
                                    source={{ uri: this.state.image }}
                                    style={styles.imageOfPlace}
                                >
                                    <Text style={styles.nameOfPlace}>
                                        {this.state.name}
                                    </Text>
                                    <Text style={styles.addressOfPlace}>
                                        {this.state.address}
                                    </Text>
                                </ImageBackground>
                            </View>
                            <View style={styles.containerBottom}>

                                <Text style={styles.descriptionOfPlace}>
                                    {this.state.description}
                                </Text>
                                <View style={styles.goButton}>
                                    <Button
                                        colorbutton="yellow"
                                        onPress={this.state.activeClaimButton ? () => this.validateUserLocation() : () => {/*Do nothing if the user was there already*/ }}
                                    >
                                        {this.state.activeClaimButton ? stringSelector('claimUrbees') + " " + this.state.urbees + " " + stringSelector('get_urbees_part2') : stringSelector('already_claimed_button_place')}
                                    </Button>
                                </View>
                                <View style={styles.twoButtonsAtBottom}>
                                    <Button colorbutton="light_orange_small" onPress={this.openGps}>{stringSelector('viewLocation')}</Button>
                                    <View style={styles.buttonSeparator}>
                                    </View>
                                    <Button colorbutton="orange_small" onPress={this.loadLinkInBrowser} >{stringSelector('more_info')}</Button>
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
                {//loading icon only shown when "ready" variable is false
                    !this.state.ready && (
                        <View style={styles.loadingIcon}>
                            <ActivityIndicator size='large' color='#FFFC71' />
                        </View>
                    )}
            </View >

        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
    },
    containerTop: {
        flex: 60,
    },
    imageOfPlace: {
        resizeMode: "cover",
        justifyContent: "flex-end",
        flex: 1,
        height: 300,
    },
    containerBottom: {
        flex: 40,
        alignItems: 'center',
        justifyContent: "center",

    },
    nameOfPlace: {

        fontSize: 30,
        fontWeight: 'bold',
        color: "white",
        marginLeft: 10,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10,
    },
    addressOfPlace: {
        color: "white",
        fontSize: 20,
        marginLeft: 10,
        marginBottom: 3,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10
    },
    descriptionOfPlace: {
        width: '80%',
        textAlign: 'justify',
        fontSize: 25,
        marginHorizontal: 5,
        marginTop: 15,
        marginBottom: 15,
    },
    goButton: {
        flexDirection: "row",
    },
    twoButtonsAtBottom: {
        flexDirection: "row",
        marginTop: 15,
        marginBottom: 15,
        alignSelf: 'center',
    },
    moreInfoButton: {
        width: '30%',
        height: 100,
        alignSelf: 'center',
    },
    locationButton: {
        width: '30%',
        height: 100,
        alignSelf: 'center',

    },
    modalBackground: {
        backgroundColor: '#000000aa',
        flex: 1
    },
    modal: {
        width: "80%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        justifyContent: "center",
        //alignContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    closeModal: {
        position: 'absolute',
        top: 30,
        right: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.1)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    wariningModalText: {
        fontSize: 20,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    modalText: {
        fontSize: 17,
    },
    modalButton: {
        backgroundColor: 'yellow',
        borderRadius: 10,
        marginHorizontal: 10,
        marginBottom: 10,
    },
    loadingIcon: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    back: {
        zIndex: 1,
        position: 'absolute',
        top: 15,
        left: 15,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonSeparator: {
        marginHorizontal: 3,
    },
    containerOfContainers: {
        flex: 1,
    },
    scrollView: {
        flex: 85,
    },
    modalSeparator: {
        flex: 2,
    },
});