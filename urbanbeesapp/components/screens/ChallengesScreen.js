import React, { useState } from 'react';
import {  StyleSheet, Text, View, TouchableOpacity, LayoutAnimation,FlatList, Image, ImageBackground, EventEmitter, Modal  } from 'react-native';
import {createAppContainer} from 'react-navigation';
import AppNavigator from '../items/challengesNavigator';
import { AntDesign } from '@expo/vector-icons';
import * as firebase from 'firebase';
import fire from '../../config';
import { Button } from '../Button';
import stringSelector from '../lenguage/lenguages';
const ChallengesMenu = createAppContainer(AppNavigator);
export default class ChallengesScreen extends React.Component {
   
    static navigationOptions = {
        headerShown: false
    };  
    state ={
        showWarningModal:false,
        ModalMessage:'',
     };
    componentDidMount(){
        fire.database().ref('/Users/'+firebase.auth().currentUser.uid).on('value', snapshot =>{
            let firstttimeUser= snapshot.child('firsttimechallenges').val();
            if(firstttimeUser=='yes'){
                this.setState({ModalMessage:stringSelector('welcome_title_challenge')+' '+stringSelector('welcome_text_challenge')});
                this.setState({showWarningModal:true});
                snapshot.child('firsttimechallenges').ref.set('no');
            }
        });
    }
    render(){ 
        //This is for the uper buttons of the challenges to show each one 
        LayoutAnimation.easeInEaseOut();
        return( 
        <View style={styles.container}>
            <Modal 
                transparent={true}
                visible={this.state.showWarningModal}
            >
                <View style={styles.questionModalBackground}>
                    <View style={styles.modalSeparator}></View>
                    <View style={styles.questionModal}>
                        <TouchableOpacity style={styles.closeQuestionModal} onPress={() => this.setState({showWarningModal:false})}>
                            <AntDesign name="closecircleo" size={24} color="black" />
                        </TouchableOpacity>
                        <Text style={styles.wariningModalText}>
                            {this.state.ModalMessage} 
                        </Text>
                        <Button onPress={() => this.setState({ showWarningModal: false })} colorbutton="yellow_small"> 
                            <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                        </Button> 
                    </View>
                    <View style={styles.modalSeparator}></View>
                </View>
            </Modal>
            <ChallengesMenu />
        </View>
        );
    }  

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF"
      },
      questionModalBackground:{
        backgroundColor:'#000000aa',
        flex:1
    },
    questionModal:{
        width: "80%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    closeQuestionModal:{
        position: 'absolute',
        top: 30,
        right: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.1)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalTitle:{
        fontSize:20,
    },
    modalquestion:{
        fontSize:15, 
        marginVertical:10,
    },
    questionModalButtons:{
        alignItems:'center',
    },
    modalText:{
        fontSize:15, 
    },
    modalButton:{
        backgroundColor:'yellow',
        borderRadius : 10,
        marginHorizontal:10,
        marginBottom:10,
    },
    wariningModalText:{
        fontSize:15,  
        color: 'black',
        width:"80%",
    },
    modalSeparator:{
        flex: 1,
    }
});