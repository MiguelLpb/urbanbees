import React, { useState } from 'react';
import { StyleSheet, Text, ScrollView, View, ImageBackground, Image, Linking, Modal, TouchableOpacity } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { Button } from '../Button';
import { Ionicons } from '@expo/vector-icons';
import * as firebase from 'firebase';
import fire from '../../config';
import stringSelector from '../lenguage/lenguages';
export default class SpecificProductScreen extends React.Component {
    static navigationOptions = {
        headerShown: false
    };
    state = {
        productId: '',
        amountOfProductLeft: '',
        name: '',
        addressUrl: '',
        description: '',
        expired: '',
        image: 'https://firebasestorage.googleapis.com/v0/b/urbanbees-e9017.appspot.com/o/Atmospheric%20CO2%20in%20years%20-%20Graph.jpg?alt=media&token=c9b0f816-3c2b-4de3-9cc0-4f0a97c342d8',
        link: '',
        urbees: '',
        type: '',
        city: '',
        endDate: '',
        provider: '',
        downloadLink: '',
        mainButtonText: stringSelector('BuyProduct'),
        infoButtonText: stringSelector('more_info'),
        extrainfoText: '',
        confirmationModal: false,
        modalMessage: '',
        modalMessage2:stringSelector("youwintext"),
        confirmationMessage: stringSelector('buy_product_download_sure_text'),
        showWarningModal: false,
        showCongratsModal: false,
        showCongratsModal2:false,
        showLocationForProduct: true,
        isRaffle: false,
        winnerCheck: false,
    };
    //Get all the info from this product from firebase
    getProduct = () => {
        var IdofUser = firebase.auth().currentUser.uid;
        var refference = fire.database().ref('/Products/' + this.state.productId);
        if (!(this.state.city == 'national')) {
            refference = fire.database().ref('/Cities/' + this.state.city + '/Products/' + this.state.productId)
        }
        refference.once('value', snapshot => {
            var typeProduct = snapshot.child('type').val();
            this.setState(
                {
                    name: snapshot.child('name').val(),
                    addressUrl: snapshot.child('address').val(),
                    description: snapshot.child('description').val(),
                    amountOfProductLeft: snapshot.child('amount').val(),
                    image: snapshot.child('image').val(),
                    type: typeProduct,
                    endDate: snapshot.child('valid_until').val(),
                    link: snapshot.child('link').val(),
                    urbees: snapshot.child('urbees').val(),
                    provider: snapshot.child('provider').val(),
                    downloadLink: snapshot.child('download_link').val(),
                }, () => {
                    if (typeProduct == 'send') {
                        this.setState({ extrainfoText: stringSelector('producttypetext_send') });
                    } else if (typeProduct == 'download') {
                        this.setState({ extrainfoText: stringSelector('producttypetext_download') });
                    } else if (typeProduct == 'link') {
                        this.setState({ extrainfoText: stringSelector('producttypetext_link') });
                        this.setState({ showLocationForProduct: false });
                    } else if (typeProduct == 'link_code') {
                        this.setState({ extrainfoText: stringSelector('producttypetext_link') });
                        this.setState({ showLocationForProduct: false });
                    } else if (typeProduct == 'claim') {
                        this.setState({ extrainfoText: stringSelector('producttypetext_claim') });
                    } else if (typeProduct == 'raffle') {
                        this.setState({ maextrainfoTextinButtonText: stringSelector('producttypetext_raffle') });
                        this.setState({ mainButtonText: stringSelector('producttypetext_raffle') });//When a raffle the user can either raffle or buy the product 
                        this.setState({ infoButtonText: stringSelector('BuyProduct') });
                        this.setState({ showLocationForProduct: false });
                        this.setState({ isRaffle: true });
                        this.setState({ confirmationMessage: stringSelector('raffle_product_sure_text') + this.state.urbees + ' Urbees?' });

                        let dateEndString = this.state.endDate;
                        let dateEndParts = dateEndString.split(".");
                        let dateEnd = new Date(+dateEndParts[2], dateEndParts[1] - 1, +dateEndParts[0]); //This converts our date format from the database to the format for Javascript
                        let today = new Date();

                        if (today > dateEnd) {
                            this.setState({ mainButtonText: stringSelector('seeRaffleWinner') });
                            this.setState({ winnerCheck: true });
                            this.setState({ confirmationMessage: stringSelector('seeRaffleWinnerText') });

                        } else {
                            var continueRaffle = dateEndString;
                            if (snapshot.child('users').child(IdofUser).child('raffle').exists()) {
                                var raffleTimes = snapshot.child('users').child(IdofUser).child('raffle').val();
                                this.setState({ extrainfoText: stringSelector('raffle_times_participated_text1') + raffleTimes + stringSelector('raffle_times_participated_text2') + stringSelector('raffle_time_text') + continueRaffle });
                            } else {
                                this.setState({ extrainfoText: stringSelector('producttypetext_raffle_participate') + stringSelector('raffle_time_text') + continueRaffle+stringSelector("dontwait") });
                            }
                        }
                    }

                }
            )



        });
    }
    openGps = () => {
        if (this.state.type == 'claim') {
            Linking.openURL(this.state.addressUrl);
        } else if (this.set.type == 'link') {

        }
    }
    openLink = () => {
        if (this.state.type == 'claim' || this.state.type == 'link' || this.state.type == 'link_code') {
            Linking.openURL(this.state.link);
        } else if (this.state.type == 'raffle') {
            Linking.openURL(this.state.downloadLink);
        }
    }
    buyProduct = () => {
        if (this.state.winnerCheck == false) {
            this.substracturbees();
        } else {
            this.checkWinner();
        }
        this.setState({ confirmationModal: false });
    }
    substracturbees = () => {
        var refference2 = fire.database().ref('/Products/' + this.state.productId);
        if (!(this.state.city == 'national')) {
            refference2 = fire.database().ref('/Cities/' + this.state.city + '/Products/' + this.state.productId)
        }
        if (this.state.amountOfProductLeft > 0) {
            var IdofUser = firebase.auth().currentUser.uid;
            fire.database().ref('/Users/' + IdofUser).once('value', snapshot => {
                if (snapshot.child('products').child(this.state.productId).child('code').exists()) {
                    this.setState({ modalMessage: stringSelector('congrats_claim') });
                    this.setState({ showWarningModal: true });
                } else {
                    var amountOfUserUrbees = snapshot.child('urbees').val();
                    if (amountOfUserUrbees >= this.state.urbees) {
                        var newUserUrbeesAmount = amountOfUserUrbees - this.state.urbees;
                        snapshot.child('urbees').ref.set(newUserUrbeesAmount);
                        if (!(this.state.type == 'raffle')) {
                            var newProductAmount = this.state.amountOfProductLeft - 1;

                            refference2.child('amount').set(newProductAmount);
                            this.setState({ amountOfProductLeft: newProductAmount });
                            refference2.child('users').child(IdofUser).child('bought').set('yes');
                            var timesUserbought = 1;
                            refference2.child('users').child(IdofUser).once('value', snapshotTime => {
                                if (snapshotTime.child('times').exists()) {
                                    timesUserbought = snapshotTime.child('times').val() + 1;
                                }
                                snapshotTime.child('times').ref.set(timesUserbought);
                            });
                        } else {
                            refference2.child('users').child(IdofUser).once('value', snapshot2 => {
                                if (snapshot2.child('raffle').exists()) {
                                    var timesUserRaffled = timesUserRaffled = snapshot2.child('raffle').val() + 1;
                                    snapshot2.child('raffle').ref.set(timesUserRaffled);
                                    this.setState({ extrainfoText: stringSelector('raffle_times_participated_text1') + timesUserRaffled + stringSelector('raffle_times_participated_text2') + stringSelector('raffle_time_text') + this.state.endDate });
                                } else {
                                    var timesUserRaffled = 1;
                                    snapshot2.child('raffle').ref.set(timesUserRaffled);
                                    this.setState({ extrainfoText: stringSelector('raffle_times_participated_text1') + timesUserRaffled + stringSelector('raffle_times_participated_text2') + stringSelector('raffle_time_text') + this.state.endDate });
                                }
                            });
                        }
                        //if the product is type claim, add to Myproducts
                        if (this.state.type == 'claim') {
                            //Claimed should be called actually "not claimed yet" but it is wrong in the database
                            var claimedValue = 1;
                            if (snapshot.child('products').child(this.state.productId).child('claimed').exists()) {
                                claimedValue = snapshot.child('products').child(this.state.productId).child('claimed').val() + 1;
                            }
                            snapshot.child('products').child(this.state.productId).ref.set(
                                {
                                    city: this.state.city,
                                    claimed: claimedValue,
                                    name: this.state.name,
                                    type: 'claim'
                                }
                            ).then(() => {
                                this.setState({ modalMessage: stringSelector('congrats_claim2') });
                                this.setState({ showCongratsModal: true });
                            });
                        } else if (this.state.type == 'link') {
                            Linking.openURL(this.state.downloadLink);
                        } else if (this.state.type == 'link_code') {
                            var codeForUser = '';
                            refference2.child('codes').orderByChild('used').equalTo('no').limitToFirst(1).once("value", codeTouse => {
                                codeTouse.forEach(function (codeTouseUnique) {
                                    codeForUser = codeTouseUnique.child('code').val();
                                    codeTouseUnique.child('used').ref.set('yes');
                                    codeTouseUnique.child('user').ref.set(IdofUser);
                                });
                            }).then(() => {
                                snapshot.child('products').child(this.state.productId).ref.set(
                                    {
                                        city: this.state.city,
                                        code: codeForUser,
                                        name: this.state.name,
                                        type: 'code',
                                    }
                                ).then(() => {
                                    this.setState({ modalMessage: stringSelector('congrats_claim_discount') });
                                    this.setState({ showCongratsModal: true });
                                });
                            });
                        } else if (this.state.type == 'raffle') {
                            this.setState({ modalMessage: stringSelector('raffle_participated_text_more') });
                            this.setState({ showCongratsModal: true });
                        }

                    } else {
                        if (this.state.type == 'raffle') {
                            this.setState({ modalMessage: stringSelector('not_enough_donate_title') });
                        } else {
                            this.setState({ modalMessage: stringSelector('not_enough_donate_title') });
                        }
                        this.setState({ showWarningModal: true });
                    }
                }
            });
        } else {
            refference2.child('expired').set('yes');
            this.setState({ modalMessage: stringSelector('product_not_available') });
            this.setState({ showWarningModal: true });
        }
    }
    checkWinner = () => {

        var refference3 = fire.database().ref('/Products/' + this.state.productId);
        if (!(this.state.city == 'national')) {
            refference3 = fire.database().ref('/Cities/' + this.state.city + '/Products/' + this.state.productId)
        }
        var IdofUser = firebase.auth().currentUser.uid;
        var participants = [];
        var expiredProduct = '';
        refference3.once('value', snapshotExpired => {
            expiredProduct = snapshotExpired.child('expired').val();
        }).then(() => {
            if (expiredProduct != 'yes') {
                var reference = refference3.child('users');
                reference.once('value', snapshot => {
                    var winnerOfRaffle = 'none';
                    snapshot.forEach(function (user) {
                        if (user.child('raffle').exists()) {
                            var raffleAmountUser = user.child('raffle').val();
                            if (raffleAmountUser > 0) {
                                for (var i = 0; i < raffleAmountUser; i++) {
                                    participants.push(user.key);
                                }
                                user.child('raffle').ref.set(0);
                            } else if (raffleAmountUser == -1) {
                                winnerOfRaffle = user.key;
                            }

                        }
                    });
                    if (winnerOfRaffle == 'none') {
                        if (participants.length > 0) {
                            var max = participants.length - 1;
                            var winnerPosition = Math.round(Math.random() * max);
                            var winnerOfRaffle = participants[winnerPosition];
                            snapshot.child(winnerOfRaffle).child('bought').ref.set('yes');
                            snapshot.child(winnerOfRaffle).child('raffle').ref.set(-1);
                            var timesUserWin = 1;
                            if (snapshot.child(winnerOfRaffle).child('times').exists()) {
                                timesUserWin = snapshot.child(winnerOfRaffle).child('times').val() + 1;
                            }
                            snapshot.child(winnerOfRaffle).child('times').ref.set(timesUserWin);

                        }
                    }
                    if (winnerOfRaffle == IdofUser) {
                        refference3.child('expired').set('yes');
                        this.setState({ showCongratsModal2: true });
                    } else if (winnerOfRaffle == 'none' && participants == 0) {
                        refference3.child('expired').set('yes');
                        this.setState({ modalMessage: stringSelector('youloosetext') });
                        this.setState({ showWarningModal: true });
                    } else {
                        this.setState({ modalMessage: stringSelector('youloosetext') });
                        this.setState({ showWarningModal: true });
                    }

                });


            } else {
                this.setState({ modalMessage: stringSelector('expired_text') });
                this.setState({ showWarningModal: true });
            }

        });

    };
    sendCodeByEmail = () => {

    };
    componentDidMount() {
        this.setState(
            {
                productId: this.props.navigation.getParam('productId', 'no'),
                city: this.props.navigation.getParam('city', global.cityfortheapp),
            }, () => { // this is for the getProduct function to execute after the setstate is done, otherwise it executes before the product id is set and the database returns null
                this.getProduct();
            });
    }
    render() {
        return (
            <View style={styles.container}>
                <Modal
                    transparent={true}
                    visible={this.state.confirmationModal}
                >
                    <View style={styles.modalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.modal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ confirmationModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.wariningModalText}>
                                {this.state.confirmationMessage}
                            </Text>
                            <View style={styles.modalButtons}>
                                <Button onPress={() => this.buyProduct()} colorbutton="yellow_small">
                                    <Text style={styles.modalText}>{stringSelector('yes')}</Text>
                                </Button>
                                <Button onPress={() => this.setState({ confirmationModal: false })} colorbutton="yellow_small">
                                    <Text style={styles.modalText}>{stringSelector('no')}</Text>
                                </Button>
                            </View>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.showWarningModal}
                >
                    <View style={styles.modalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.modal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ showWarningModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.wariningModalText}>
                                {this.state.modalMessage}
                            </Text>
                            <Button onPress={() => this.setState({ showWarningModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.showCongratsModal}
                >
                    <View style={styles.modalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.modal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ showCongratsModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.modalText}>
                                {this.state.modalMessage}
                            </Text>
                            <Button onPress={() => this.setState({ showCongratsModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.showCongratsModal2}
                >
                    <View style={styles.modalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.modal}>
                            <Text style={styles.modalText}>
                                {this.state.modalMessage2}
                            </Text>
                            <Button onPress={() => {this.setState({ showCongratsModal: false }); this.openLink(); }} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <TouchableOpacity style={styles.back} onPress={() => this.props.navigation.goBack()}>
                    <Ionicons name='ios-arrow-back' size={32} color='black'></Ionicons>
                </TouchableOpacity>
                <View style={styles.scrollView}>

                    <ScrollView style={styles.scrollViewScroll}>
                        <View style={styles.containerOfContainers}>
                            <View style={styles.containerTop}>
                                <ImageBackground
                                    source={{ uri: this.state.image }}
                                    style={styles.imageOfProduct}
                                >
                                    <Text style={styles.nameOfProduct}>
                                        {this.state.name}
                                    </Text>
                                    <Text style={styles.providerOfProduct}>
                                        {this.state.provider}
                                    </Text>
                                </ImageBackground>
                            </View>
                            <View style={styles.containerBottom}>
                                <Text style={styles.productDescriptionTitle}>
                                    {stringSelector("descriptionTittleText")}
                                </Text>
                                <Text style={styles.productDescription}>
                                    {this.state.description}
                                </Text>
                                <Text style={styles.productDescriptionTitle}>
                                    {stringSelector("HowToGetTittleText")}
                                </Text>
                                <Text style={styles.productDescription}>
                                    {this.state.extrainfoText}
                                </Text>
                                <View style={styles.twoButtonsAtBottom}>
                                    <Button colorbutton="orange_small" onPress={this.openLink} >{this.state.infoButtonText}</Button>
                                    {//If the product is not for claiming in store, dont show location button
                                        this.state.showLocationForProduct && (
                                            <TouchableOpacity style={styles.locationButton} onPress={this.openGps}>
                                                <Image
                                                    source={require('../../assets/location.png')}
                                                    resizeMode='contain'
                                                    style={styles.urbeeCoin}
                                                ></Image>
                                                <Text style={styles.locationButtonText}>{stringSelector("viewLocation")}</Text>
                                            </TouchableOpacity>
                                        )}
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
                <View style={styles.priceAndBuyButton}>
                    <Image
                        source={require('../../assets/urbeecriptocurrency.png')}
                        resizeMode='contain'
                        style={styles.urbeeCoin}
                    ></Image>
                    <Text style={styles.price}>
                        {this.state.urbees}
                    </Text>
                    <Button colorbutton="white_small" onPress={() => this.setState({ confirmationModal: true })} >
                        {this.state.mainButtonText}
                    </Button>
                </View>
            </View >
        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
    },
    containerOfContainers: {
        flex: 1,
    },
    containerTop: {
        flex: 35,
    },
    scrollView: {
        flex: 85,
    },
    imageOfProduct: {
        resizeMode: "cover",
        justifyContent: "space-between",
        flex: 1,
        flexDirection: "column-reverse",
        height: 300,
    },
    containerBottom: {
        flex: 65,
        alignItems: "center",
    },
    nameOfProduct: {
        fontSize: 21,
        fontWeight: 'bold',
        color: "white",
        marginStart: 10,
        textShadowColor: 'rgba(0, 0, 0, 1)',
        textShadowOffset: { width: 1, height: 1 },
        textShadowRadius: 3,

    },
    providerOfProduct: {
        fontSize: 15,
        fontWeight: 'bold',
        color: "white",
        alignSelf: "flex-end",
        marginEnd: 5,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10,

    },
    productDescription: {
        width: '90%',
        color: "black",
        fontSize: 20,
        marginBottom: 3,
        textAlign: "justify",
    },
    productDescriptionTitle: {
        color: "black",
        fontSize: 25,
        marginBottom: 3,
        textAlign: "center",
        fontWeight: 'bold',
    },
    priceAndBuyButtonContainer: {
        flexDirection: "row",
        alignItems: "center",
    },
    price: {
        fontSize: 30,
        fontWeight: "bold",
        marginEnd: 10,

    },
    twoButtonsAtBottom: {
        flexDirection: "row",
        marginHorizontal: 15,
        justifyContent: "center",
        marginBottom: 5,
    },
    moreInfoButton: {
        width: '30%',
        height: 100,
        alignSelf: 'center',
    },

    locationButton: {
        alignItems: "center",
        justifyContent: "center",
        flexDirection: "row",
        marginStart: 10,
        marginTop: 10,
        width: '39%',
        borderRadius: 20,
        paddingVertical: 10,
        alignItems: 'center',
        backgroundColor: '#ffb571'
    },
    modalBackground: {
        backgroundColor: '#000000aa',
        flex: 1
    },
    modal: {
        width: "80%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    closeModal: {
        position: 'absolute',
        top: 30,
        right: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.1)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalButtons: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    modalButton: {
        backgroundColor: 'yellow',
        flex: 1,
        borderRadius: 10,
        marginHorizontal: 10,
        marginBottom: 10,
    },
    wariningModalText: {
        fontSize: 20,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    modalText: {
        fontSize: 20,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    priceAndBuyButton: {
        flex: 15,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#fffc71",
        paddingBottom: 7,
    },
    urbeeCoin: {
        width: 20,
        height: 20,
        alignSelf: "center",
        marginEnd: 10,
    },
    locationButtonText: {
        fontWeight: '700',
        fontSize: 18,
        color: "black"
    },
    back: {
        zIndex: 1,
        position: 'absolute',
        top: 10,
        left: 15,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalSeparator: {
        flex: 4,
    },
});