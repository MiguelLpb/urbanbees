import React, { useState } from 'react';
import {  StyleSheet, Text, View, TouchableOpacity, LayoutAnimation,FlatList, Image, ImageBackground  } from 'react-native';
import * as firebase from 'firebase';
import fire from '../../config';
export default class MyProductsScreen extends React.Component {
    state ={
        userId:'',
        myProducts: [],
        type:''
     };
    static navigationOptions = {
        headerShown: false
    };  
     ///function to get products that a person already payed from firebase
     getMyProducts= () => {
         //Use the ref from the begining of the database to be able to navigate to get the image from the list of products in the city
        fire.database().ref().on('value', snapshot =>{
            var myProductsFromDatabase=[];
            snapshot.child('Users').child(firebase.auth().currentUser.uid).child('products').forEach(function(myProduct) {
                var productType= myProduct.child('type').val();
                if(productType=='code'){
                        myProductsFromDatabase.push(
                            {
                                id:myProduct.key,
                                name: myProduct.child("name").val(),
                                city: myProduct.child('city').val(),
                                image:snapshot.child('Products').child(myProduct.key).child('image').val(),
                                type:productType,
                            }
                        );
                }else{
                    var claimedAmout= myProduct.child('claimed').val();
                    if(claimedAmout>0){
                        for(var i=0;i<claimedAmout;i++){
                            myProductsFromDatabase.push(
                                {
                                    id:myProduct.key,
                                    name: myProduct.child("name").val(),
                                    city: myProduct.child('city').val(),
                                    image:snapshot.child('Cities').child(myProduct.child('city').val()).child('Products').child(myProduct.key).child('image').val(),
                                    type:'claim',
                                }
                            );
                        }
                    }
                }
            });
            this.setState({myProducts:myProductsFromDatabase});
        });
     }
     componentDidMount(){
        const {userId}= firebase.auth().currentUser.uid;
        this.setState({userId});
        this.getMyProducts();
    }
//This is the design of each part of the scrollview 
renderMyProducts= specificProduct =>{
    return(
      <View style={styles.myProduct}>
          <TouchableOpacity style={styles.organizeitems} onPress={()=>this.props.navigation.navigate("MyProduct", {productId: specificProduct.id, type: specificProduct.type, cityofproduct:specificProduct.city })}>
            <ImageBackground
            source={{uri:specificProduct.image}}
            style={styles.imageOfmyProducts}
            >
            <View style={styles.textPorduct}>
                <Text style={ styles.nameText}>{specificProduct.name}</Text>
            </View> 
            </ImageBackground>
        </TouchableOpacity>
      </View> 
    );
};

    render(){ 
        //This is for the design outside of the scroll view
        LayoutAnimation.easeInEaseOut();
        return(
        <View style={styles.container}>
            <View style={styles.topmargin}>
                <FlatList 
                    style={styles.feed}
                    data={this.state.myProducts}
                    renderItem={({item})=>this.renderMyProducts(item)}
                    keyExtractor={item=>item.id}
                    showsVerticalScrollIndicator={false}
                />
            </View>
          </View>
        );
    }  

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF"
      },
      topmargin:{
          marginTop:10,
      },
      myProduct:{
       marginBottom: 10,   
       borderColor:"black",
       borderWidth:3, 
       borderRadius: 10, 
       flex:1,
       marginHorizontal:20,
      },
      nameText:{
       fontSize:18,
       fontWeight: "700",
       color:"white",
       marginLeft:10,
       textShadowColor: 'rgba(0, 0, 0, 0.75)',
       textShadowOffset: {width: -1, height: 1},
       textShadowRadius: 10
      },
      textPorduct:{
        flexDirection:"column-reverse",
        flex:1,
        },
      organizeitems:{
       flexDirection: "row",
      },
      imageOfmyProducts: {
        resizeMode: "cover",
        flexDirection:"column-reverse",
        justifyContent: "flex-start",
        overflow: "hidden",
        borderRadius: 5,
        flex:1,
        height:150,
    },
});