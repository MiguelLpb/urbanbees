import React, { useState } from 'react';
import { StyleSheet, Text, View, ImageBackground, ScrollView, Image, Linking, Modal, TouchableOpacity } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { Button } from '../Button';
import * as firebase from 'firebase';
import fire from '../../config';
import stringSelector from '../lenguage/lenguages';
export default class SpecificMyProductScreen extends React.Component {
    static navigationOptions = {
        headerShown: false
    };
    state = {
        myProductId: '',
        myProductType: '',
        name: '',
        addressUrl: '',
        description: '',
        image: 'https://firebasestorage.googleapis.com/v0/b/urbanbees-e9017.appspot.com/o/Atmospheric%20CO2%20in%20years%20-%20Graph.jpg?alt=media&token=c9b0f816-3c2b-4de3-9cc0-4f0a97c342d8',
        link: '',
        urbees: '',
        city: global.cityfortheapp,
        cityofproduct: '',
        provider: '',
        mainButtonText: stringSelector("ClaimButtonText"),
        infoButtonText: 'Info',
        extraInfoText: '',
        confirmationModal: false,
        confirmationMessage: stringSelector('claim_product_sure_text'),
        modalMessage: '',
        modalMessage2: '',
        showWarningModal: false,
        showWarningModal2: false,
        showCongratsModal: false,
    };
    //Get all the info from this product from firebase
    //text for confirmationMessage: Are you sure you want to claim this product now?. The provider of the product must tap on this button to confirm the transaction. Simply go to the shop and show this to a member of the staff. Do not press the button yourself otherwise the product will be considered as delivered and the product will be removed from your inventory. Continue and claim?
    getProduct = () => {
        if (this.state.myProductType == 'code') {
            fire.database().ref('/Products/' + this.state.myProductId).once('value', snapshot => {
                this.setState(
                    {
                        name: snapshot.child('name').val(),
                        description: snapshot.child('description').val(),
                        image: snapshot.child('image').val(),
                        link: snapshot.child('link').val(),
                        urbees: snapshot.child('urbees').val(),
                        provider: snapshot.child('provider').val(),
                    }
                )
            });

        } else {
            fire.database().ref('/Cities/' + this.state.cityofproduct + '/Products/' + this.state.myProductId).once('value', snapshot => {
                this.setState(
                    {
                        name: snapshot.child('name').val(),
                        addressUrl: snapshot.child('address').val(),
                        description: snapshot.child('description').val(),
                        image: snapshot.child('image').val(),
                        link: snapshot.child('link').val(),
                        urbees: snapshot.child('urbees').val(),
                        provider: snapshot.child('provider').val(),
                    }
                )
            });
        }
    }
    openGps = () => {
        Linking.openURL(this.state.addressUrl);
    }
    openLink = () => {
        Linking.openURL(this.state.link);
    }
    claimProduct = () => {
        this.setState({ confirmationModal: false });
        var IdofUser = firebase.auth().currentUser.uid;
        if (this.state.myProductType == 'code') {
            var discountCode = '';
            var Url = '';
            fire.database().ref('/Users/' + IdofUser).once('value', snapshot => {
                discountCode = snapshot.child('products').child(this.state.myProductId).child('code').val();
                Url = this.state.link;
            }).then(() => {
                this.setState({ modalMessage2:  stringSelector("yourcode")+discountCode  });
                this.setState({ showWarningModal2: true });
            });

        } else {
            fire.database().ref('/Users/' + IdofUser).once('value', snapshot => {
                var claimedValue = snapshot.child('products').child(this.state.myProductId).child('claimed').val();
                if (claimedValue > 0) {
                    snapshot.child('products').child(this.state.myProductId).child('claimed').ref.set(claimedValue - 1).then(() => {
                        this.setState({ modalMessage: stringSelector('congrats_claimed_now') });
                        this.setState({ showCongratsModal: true });
                    });
                } else {
                    this.setState({ modalMessage: stringSelector('no_products_to_claim') });
                    this.setState({ showWarningModal: true });
                }

            });
        }
    }
    componentDidMount() {
        this.setState(
            {
                myProductId: this.props.navigation.getParam('productId', 'no'),
                myProductType: this.props.navigation.getParam('type', 'no'),
                cityofproduct: this.props.navigation.getParam('cityofproduct', 'national')
            }, () => { // this is for the getProduct function to execute after the setstate is done, otherwise it executes before the product id is set and the database returns null
                this.getProduct();
            });
    }
    render() {
        return (
            <View style={styles.container}>
                <Modal
                    transparent={true}
                    visible={this.state.confirmationModal}
                >
                    <View style={styles.modalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.modal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ confirmationModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.modalTextLong}>
                                {this.state.confirmationMessage}
                            </Text>
                            <View style={styles.modalButtons}>
                                <Button onPress={() => this.claimProduct()} colorbutton="yellow_small">
                                    <Text style={styles.modalText}>{stringSelector('yes')}</Text>
                                </Button>
                                <Button onPress={() => this.setState({ confirmationModal: false })} colorbutton="yellow_small">
                                    <Text style={styles.modalText}>{stringSelector('no')}</Text>
                                </Button>
                            </View>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.showWarningModal}
                >
                    <View style={styles.modalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.modal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ showWarningModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.wariningModalText}>
                                {this.state.modalMessage}
                            </Text>
                            <Button onPress={() => this.setState({ showWarningModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.showWarningModal2}
                >
                    <View style={styles.modalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.modal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ showWarningModal2: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.wariningModalText} selectable >
                                {this.state.modalMessage2}
                            </Text>
                            <Button onPress={() => this.setState({ showWarningModal2: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.showCongratsModal}
                >
                    <View style={styles.modalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.modal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ showCongratsModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.modalText}>
                                {this.state.modalMessage}
                            </Text>
                            <Button onPress={() => this.setState({ showCongratsModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <View style={styles.scrollView}>
                    <TouchableOpacity style={styles.back} onPress={() => this.props.navigation.goBack()}>
                        <Ionicons name='ios-arrow-back' size={32} color='black'></Ionicons>
                    </TouchableOpacity>
                    <ScrollView style={styles.scrollViewScroll}>
                        <View style={styles.containerOfContainers}>
                            <View style={styles.containerTop}>
                                <ImageBackground
                                    source={{ uri: this.state.image }}
                                    style={styles.imageOfProduct}
                                >
                                    <Text style={styles.nameOfProduct}>
                                        {this.state.name}
                                    </Text>
                                    <Text style={styles.productProvider}>
                                        {this.state.provider}
                                    </Text>
                                </ImageBackground>
                            </View>
                            <View style={styles.containerBottom}>
                                <Text style={styles.productDescriptionTitle}>
                                    {stringSelector("descriptionTittleText")}
                                </Text>
                                <Text style={styles.productDescription}>
                                    {this.state.description}
                                </Text>
                                <Text style={styles.price}>
                                    {stringSelector("youspendText") + " " + this.state.urbees}
                                </Text>
                                {//If the product is for code, show this
                                        (this.state.myProductType == 'code') && (
                                <Text style={styles.productDescription}>
                                    {stringSelector("howtoclaim")}
                                </Text>
                                 )}
                                <View style={styles.twoButtonsAtBottom}>
                                    <Button colorbutton="orange_small" onPress={this.openLink} >{stringSelector("more_info")}</Button>
                                    <View style={styles.buttonSeparator}></View>
                                    {//If the product is not for code, show this
                                        !(this.state.myProductType == 'code') && (
                                            <Button colorbutton="yellow_small" onPress={this.openGps} >Location</Button>
                                        )}
                                </View>
                            </View>
                        </View>
                    </ScrollView>
                </View>
                <View style={styles.claimAndBuyButton}>
                    <Button colorbutton="white_small" onPress={() => {if(this.state.myProductType == 'code'){this.claimProduct(); }else{this.setState({ confirmationModal: true })} }}>
                        {this.state.myProductType == 'code' ? stringSelector("showCodeButtonText") : this.state.mainButtonText}
                    </Button>
                </View>
            </View >
        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
    },
    containerTop: {
        flex: 35,

    },
    scrollView: {
        flex: 1,
    },
    containerOfContainers: {
        flex: 1,
    },
    imageOfProduct: {
        resizeMode: "cover",
        flex: 1,
        height: 300,
        flexDirection: "row",
        justifyContent: "space-between",
    },
    claimAndBuyButton: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#fffc71",
        paddingBottom: 8,
    },
    containerBottom: {
        flex: 65,
        justifyContent: "center",
        alignItems: "center",
    },
    nameOfProduct: {
        fontSize: 18,
        fontWeight: 'bold',
        color: "white",
        marginStart: 10,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10,
        alignSelf: "flex-end",
    },
    productProvider: {
        fontSize: 18,
        fontWeight: 'bold',
        color: "white",
        marginEnd: 10,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10
    },
    productDescription: {
        color: "black",
        fontSize: 15,
        marginBottom: 3,
        textAlign: "center",
        width: "80%",
    },
    price: {
        fontSize: 15,
        marginEnd: 5,
    },
    twoButtonsAtBottom: {
        flexDirection: "row",
        marginBottom: 5,
    },
    moreInfoButton: {
        width: '30%',
        height: 100,
        alignSelf: 'center',
    },

    locationButton: {
        width: '30%',
        height: 100,
        alignSelf: 'center',

    },
    modalBackground: {
        backgroundColor: '#000000aa',
        flex: 1
    },
    modal: {
        width: "80%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    closeModal: {
        position: 'absolute',
        top: 30,
        right: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.1)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalButtons: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    modalButton: {
        backgroundColor: 'yellow',
        flex: 1,
        borderRadius: 10,
        marginHorizontal: 10,
        marginBottom: 10,
    },
    wariningModalText: {
        fontSize: 20,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    modalText: {
        fontSize: 17,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    modalTextLong: {
        fontSize: 13,
        fontWeight: 'bold',
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    back: {
        zIndex: 1,
        position: 'absolute',
        top: 10,
        left: 15,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    urbeeCoin: {
        width: 20,
        height: 20,
        alignSelf: "center",
        marginEnd: 10,
    },
    productDescriptionTitle: {
        color: "black",
        fontSize: 15,
        marginBottom: 3,
        textAlign: "center",
        fontWeight: 'bold',
    },
    buttonSeparator: {
    },
    modalSeparator: {
        flex: 4,
    },
});