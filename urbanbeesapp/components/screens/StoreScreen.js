import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, LayoutAnimation, FlatList, Image, ImageBackground, Modal, Linking } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import * as firebase from 'firebase';
import fire from '../../config';
import { Button } from '../Button';
import stringSelector from '../lenguage/lenguages';
export default class StoreScreen extends React.Component {
    state = {
        userId: '',
        city: global.cityfortheapp,
        products: [],
        honeyBarImage: require('../../assets/honeybar/donationbar0.png'),
        honeyBarAmount: 0,
        showDonationModal: false,
        showWarningModal: false,
        showCongratsModal: false,
        ModalMessage: stringSelector('not_enough_donate_title'),
    };
    ///function to get products from firebase
    getProducts = () => {
        var countryProducts = [{ id: "beep", name: 'honey bar', urbees: 0, image: 'none', city: 'farfetch' }];
        var cityofProducts = this.state.city;
        fire.database().ref('/Products/').once('value', snapshot => {
            var countryProductsFromDatabase = countryProducts; //add first item as a dummy (wont be rendered because the item of index 0 will be substituted for the honey bar)
            snapshot.forEach(function (product) {
                if (product.child('expired').val() == 'no' && product.child('amount').val() > 0 && product.child('urbees').val() != -1) {
                    let productDateString = product.child('valid_until').val();
                    let productDateParts = productDateString.split(".");
                    let productDate = new Date(+productDateParts[2], productDateParts[1] - 1, +productDateParts[0]); //This converts our date format from the database to the format for Javascript
                    let today = new Date();
                    if (today < productDate) {
                        countryProductsFromDatabase.push(
                            {
                                id: product.key,
                                name: product.child("name").val(),
                                urbees: product.child("urbees").val(),
                                image: product.child("image").val(),
                                city: 'national',
                            }
                        );
                    } else {
                        if (!(product.child('type').val() == 'raffle')) {
                            product.child('expired').ref.set('yes');
                        } else {
                            countryProductsFromDatabase.push(
                                {
                                    id: product.key,
                                    name: product.child("name").val(),
                                    urbees: product.child("urbees").val(),
                                    image: product.child("image").val(),
                                    city: 'national',
                                }
                            );
                        }
                    }
                }
            });
            countryProducts = countryProductsFromDatabase;

        }).then(() => {
            fire.database().ref('/Cities/' + this.state.city + '/Products').once('value', snapshot => {
                var productsFromDatabase = countryProducts; //add first item as a dummy (wont be rendered because the item of index 0 will be substituted for the honey bar)
                //if(snapshot.child('active')=="yes"){
                snapshot.forEach(function (product) {
                    if (product.child('expired').val() == 'no' && product.child('amount').val() > 0 && product.child('urbees').val() != -1) {
                        let productDateString = product.child('valid_until').val();
                        let productDateParts = productDateString.split(".");
                        let productDate = new Date(+productDateParts[2], productDateParts[1] - 1, +productDateParts[0]); //This converts our date format from the database to the format for Javascript
                        let today = new Date();
                        if (today < productDate) {
                            productsFromDatabase.push(
                                {
                                    id: product.key,
                                    name: product.child("name").val(),
                                    urbees: product.child("urbees").val(),
                                    image: product.child("image").val(),
                                    city: cityofProducts,
                                }
                            );
                        } else {
                            if (!(product.child('type').val() == 'raffle')) {
                                product.child('expired').ref.set('yes');
                            } else {
                                productsFromDatabase.push(
                                    {
                                        id: product.key,
                                        name: product.child("name").val(),
                                        urbees: product.child("urbees").val(),
                                        image: product.child("image").val(),
                                        city: cityofProducts,
                                    }
                                );
                            }
                        }
                    }
                });
                productsFromDatabase.push([{ id: "beep2", name: 'ContactUs', urbees: 0, image: 'none', city: 'farfetch' }]);
                this.setState({ products: productsFromDatabase });
            });

        });


    }
    //function to retrieve the amount of honey that is already donated, and set the bar with the correct image
    setHoneyBarData = () => {
        fire.database().ref('/Donations/').on('value', snapshot => {
            let donations = snapshot.child('Amount').val();
            if (donations >= 252000) {
                this.setState({ honeyBarAmount: 100 });
            } else {
                this.setState({ honeyBarAmount: Math.floor((donations * 100) / 252000) });
            }
            if (donations < 25200) {
                this.setState({ honeyBarImage: require('../../assets/honeybar/donationbar1.png') });
            } else if (donations < 50400) {
                this.setState({ honeyBarImage: require('../../assets/honeybar/donationbar2.png') });
            } else if (donations < 75600) {
                this.setState({ honeyBarImage: require('../../assets/honeybar/donationbar3.png') });
            } else if (donations < 100800) {
                this.setState({ honeyBarImage: require('../../assets/honeybar/donationbar4.png') });
            } else if (donations < 126000) {
                this.setState({ honeyBarImage: require('../../assets/honeybar/donationbar5.png') });
            } else if (donations < 151200) {
                this.setState({ honeyBarImage: require('../../assets/honeybar/donationbar6.png') });
            } else if (donations < 176400) {
                this.setState({ honeyBarImage: require('../../assets/honeybar/donationbar7.png') });
            } else if (donations < 201600) {
                this.setState({ honeyBarImage: require('../../assets/honeybar/donationbar8.png') });
            } else if (donations < 226800) {
                this.setState({ honeyBarImage: require('../../assets/honeybar/donationbar9.png') });
            } else if (donations >= 252000) {
                this.setState({ honeyBarImage: require('../../assets/honeybar/donationbar10.png') });
            }
        });
    };
    //Function that executes when clicking the donation bar
    donationFunctionality = () => {
        var amountDonated = 30;
        var IdofUser = firebase.auth().currentUser.uid;
        this.setState({ showDonationModal: false });
        fire.database().ref('/Users/' + IdofUser).once('value', snapshot => {
            if (snapshot.child('urbees').val() > amountDonated) {
                let newUrbees = snapshot.child('urbees').val() - amountDonated;
                snapshot.child('urbees').ref.set(newUrbees);
                firebase.database().ref("/Donations/").once('value', snapshot2 => {
                    let newDonationAmount = snapshot2.child('Amount').val() + amountDonated;
                    snapshot2.child('Amount').ref.set(newDonationAmount).then(function () {
                        if (snapshot2.child('Users').child(IdofUser).exists()) {
                            let newDonationFromUser = snapshot2.child('Users').child(IdofUser).val() + amountDonated;
                            snapshot2.child('Users').child(IdofUser).ref.set(newDonationFromUser);
                        } else {
                            snapshot2.child('Users').child(IdofUser).set(amountDonated);
                        }
                    });

                });
                this.setState({ showCongratsModal: true });
            } else {
                this.setState({ ModalMessage: stringSelector('not_enough_donate_title') });
                this.setState({ showWarningModal: true });
            }

        });
    };
    componentDidMount() {
        const { userId } = firebase.auth().currentUser.uid;
        this.setState({ userId });
        fire.database().ref('/Users/' + firebase.auth().currentUser.uid).on('value', snapshot => {
            let firstttimeUser = snapshot.child('firsttimestore').val();
            if (firstttimeUser == 'yes') {
                this.setState({ ModalMessage: stringSelector('welcome_title_store') + ' ' + stringSelector('welcome_text_store') });
                this.setState({ showWarningModal: true });
                snapshot.child('firsttimestore').ref.set('no');
            }
        });
        this.getProducts();
        this.setHoneyBarData();
    }

    static navigationOptions = {
        headerShown: false
    };
    //This is the design of each part of the scrollview 
    renderProducts = (specificProdcut, index) => {
        if (index == 0) {
            return (
                <View style={styles.donationBar}>
                    <TouchableOpacity style={styles.organizeBar} onPress={() => this.setState({ showDonationModal: true })} >
                        <Text style={styles.barText}>Donate</Text>
                        <Image
                            style={styles.honeyBarImage}
                            source={this.state.honeyBarImage}
                            resizeMode='contain'
                        />
                        <Text style={styles.baramount}>{this.state.honeyBarAmount + "%"}</Text>
                    </TouchableOpacity>
                </View>
            );
        }
        if (index == this.state.products.length - 1) {
            return (
                <View style={styles.product}>
                    <TouchableOpacity style={styles.organizeitems} onPress={() => {
                        Linking.openURL("https://urbanbeespartner.typeform.com/to/VsdPiA").catch(err => console.error("Couldn't load page", err));
                    }}>
                        <ImageBackground
                            source={require("../../assets/contact/businessCardBack.png")}
                            style={styles.imageOfProduct}
                        >
                            <View style={styles.textProduct}>
                                <Text style={styles.nameText}>{stringSelector("contactUsText")}</Text>
                            </View>
                        </ImageBackground>
                    </TouchableOpacity>
                </View>
            );

        }
        return (
            <View style={styles.product}>
                <TouchableOpacity style={styles.organizeitems} onPress={() => this.props.navigation.navigate("Product", { productId: specificProdcut.id, city: specificProdcut.city })}>
                    <ImageBackground
                        source={{ uri: specificProdcut.image }}
                        style={styles.imageOfProduct}
                    >
                        <View style={styles.pricetPorduct}>
                            <Image
                                source={require('../../assets/urbeecriptocurrency.png')}
                                resizeMode='contain'
                                style={styles.urbeeCoin}
                            ></Image>
                            <Text style={styles.urbeesText}>{specificProdcut.urbees}</Text>
                        </View>
                        <View style={styles.textProduct}>
                            <Text style={styles.nameText}>{specificProdcut.name}</Text>
                        </View>
                    </ImageBackground>
                </TouchableOpacity>
            </View>
        );


    };

    render() {
        //This isfor the design outside of the scroll view
        LayoutAnimation.easeInEaseOut();
        return (
            <View style={styles.container}>
                <Modal
                    transparent={true}
                    visible={this.state.showDonationModal}
                >
                    <View style={styles.donationModalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.donationModal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ showDonationModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.modalTitle}>
                            You can make a difference!
                            </Text>
                            <Text style={styles.modalText}>
                                {stringSelector('donate_text')}
                            </Text>
                            <View style={styles.donationModalButtons}>
                                <Button onPress={() => this.donationFunctionality()} colorbutton="yellow_small">
                                    <Text style={styles.modalText}>{stringSelector('yes')}</Text>
                                </Button>
                                <Button onPress={() => this.setState({ showDonationModal: false })} colorbutton="yellow_small">
                                    <Text style={styles.modalText}>{stringSelector('no')}</Text>
                                </Button>
                            </View>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.showWarningModal}
                >
                    <View style={styles.donationModalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.donationModal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ showWarningModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.wariningModalText}>
                                {this.state.ModalMessage}
                            </Text>
                            <Button onPress={() => this.setState({ showWarningModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.showCongratsModal}
                >
                    <View style={styles.donationModalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.donationModal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ showCongratsModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.modalText}>
                                {stringSelector('thanks_donate_title') + ' ' + stringSelector('thanks_donate_text')}
                            </Text>
                            <Button onPress={() => this.setState({ showCongratsModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <View style={styles.topmargin}>
                    <FlatList
                        style={styles.feed}
                        data={this.state.products}
                        renderItem={({ item, index }) => this.renderProducts(item, index)}
                        keyExtractor={item => item.id}
                        showsVerticalScrollIndicator={false}
                    />
                </View>
            </View>
        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF"
    },
    topmargin: {
        marginTop: 10,
    },
    product: {
        marginBottom: 10,
        borderColor: "black",
        borderWidth: 3,
        borderRadius: 10,
        flex: 1,
        marginHorizontal: 20,
    },
    organizeitems: {
    },
    organizeBar: {
        flexDirection: 'column',
    },
    dateText: {
        fontSize: 20,
        fontWeight: "700",
        color: "white",
        marginLeft: 20,
    },
    imageOfProduct: {
        resizeMode: "cover",
        overflow: "hidden",
        borderRadius: 5,
        flex: 1,
        height: 150,
        flexDirection: "row-reverse",
        justifyContent: "space-between",
    },
    donationBar: {
        marginBottom: 10,
        borderColor: "black",
        borderWidth: 3,
        borderRadius: 10,
        marginHorizontal: 20,
        justifyContent: "center",
        alignItems: "center",
        flexDirection: "column",
        flex: 1,
    },
    barText: {
        fontSize: 30,
        fontWeight: "700",
        color: "black",
        alignSelf: "center",
    },
    honeyBarImage: {
        width: 300,
        height: 50,
    },
    baramount: {
        fontSize: 15,
        fontWeight: "700",
        color: "black",
        alignSelf: "flex-end",
        marginRight: 10,
    },
    donationModalBackground: {
        backgroundColor: '#000000aa',
        flex: 1
    },
    donationModal: {
        width: "80%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    modalTitle: {
        width: "80%",
        fontSize: 25,
    },
    donationModalButtons: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    modalButton: {
        backgroundColor: 'yellow',
        borderRadius: 10,
        marginHorizontal: 10,
    },
    modalText: {
        fontSize: 17,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    closeModal: {
        position: 'absolute',
        top: 30,
        right: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.1)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    wariningModalText: {
        fontSize: 20,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    urbeeCoin: {
        width: 15,
        height: 15,
        alignSelf: "center",
    },
    urbeesText: {
        fontSize: 15,
        fontWeight: "700",
        color: "white",
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 1,
    },
    textProduct: {
        alignSelf: "flex-end",
        flex: 1,
    },
    nameText: {
        fontSize: 19,
        fontWeight: "700",
        color: "white",
        marginLeft: 5,
        textShadowColor: 'rgba(0, 0, 0, 1)',
        textShadowOffset: { width: 1, height: 1 },
        textShadowRadius: 3
    },
    pricetPorduct: {
        flexDirection: "row",
        alignSelf: "flex-start",
        marginEnd: 5,
    },
    modalSeparator: {
        flex: 2,
    },
});