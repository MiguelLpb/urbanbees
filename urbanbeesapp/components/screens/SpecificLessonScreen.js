import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, ScrollView, Linking, Modal, TouchableOpacity } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import HTML from 'react-native-render-html';
import { Button } from '../Button';
import * as firebase from 'firebase';
import fire from '../../config';
import stringSelector from '../lenguage/lenguages';
export default class SpecificLessonScreen extends React.Component {
    static navigationOptions = {
        headerShown: false
    };
    state = {
        lessonId: '',
        name: '',
        textUrl: '<p>text</p>',
        image: 'https://firebasestorage.googleapis.com/v0/b/urbanbees-e9017.appspot.com/o/Atmospheric%20CO2%20in%20years%20-%20Graph.jpg?alt=media&token=c9b0f816-3c2b-4de3-9cc0-4f0a97c342d8',
        link: '',
        urbees: '',
        city: global.cityfortheapp,
        expired: '',
        correctAnswer: '',
        answer1: '',
        answer2: '',
        answer3: '',
        question: '',
        showCollectButton: false,
        showQuestionModal: false,
        showWarningModal: false,
        showCongratsModal: false,
    };
    //Get all the info from this place from firebase
    getLesson = () => {
        fire.database().ref('/appnews/' + this.state.lessonId).once('value', snapshot => {
            var IdofUser = firebase.auth().currentUser.uid;
            this.setState(
                {
                    name: snapshot.child('name').val(),
                    textUrl: snapshot.child('text').val(),
                    image: snapshot.child('image').val(),
                    link: snapshot.child('link').val(),
                    urbees: snapshot.child('urbees').val(),
                    expired: snapshot.child('expired').val(),
                    correctAnswer: snapshot.child('answers').child('correct').val(),
                    answer1: snapshot.child('answers').child('1').val(),
                    answer2: snapshot.child('answers').child('2').val(),
                    answer3: snapshot.child('answers').child('3').val(),
                    question: snapshot.child('answers').child('question').val(),
                }
            )
            if (snapshot.child('users').child(IdofUser).exists()) {
                if (!(snapshot.child('users').child(IdofUser).child('claimed').val() == 'yes')) {
                    this.setState({ showCollectButton: true });
                }
            } else {
                this.setState({ showCollectButton: true });
            }
        });
    }
    loadLinkInBrowser = () => {
        Linking.openURL(this.state.link).catch(err => console.error("Couldn't load page", err));
    };
    askquestion = () => {

        this.setState({ showQuestionModal: true });
    };
    answerQuestion = (answer) => {
        var lessonId = this.state.lessonId;
        var IdofUser = firebase.auth().currentUser.uid;
        if (answer == this.state.correctAnswer) {
            fire.database().ref().once('value', snapshot => {
                //if the user exist in the lesson information and has not try before, give them the full amout of urbees
                if (snapshot.child("appnews").child(lessonId).child("users").child(IdofUser).exists()) {
                    if (snapshot.child("appnews").child(lessonId).child("users").child(IdofUser).child("try").val() == 0) {
                        let newUrbees = snapshot.child('Users').child(IdofUser).child('urbees').val() + this.state.urbees;
                        snapshot.child('Users').child(IdofUser).child('urbees').ref.set(newUrbees);
                        //level up the learningBee
                        var newlessontimes = 0;
                        if (snapshot.child('Users').child('learningtimes').exists) {
                            newlessontimes = snapshot.child('Users').child('learningtimes').val() + 1;
                        } else {
                            newlessontimes = 1;
                        }
                        snapshot.child('Users').child('learningtimes').ref.set(newlessontimes);
                    } else {
                        //otherwise give him half of the urbees
                        let newUrbees = snapshot.child('Users').child(IdofUser).child('urbees').val() + Math.round(this.state.urbees / 2);
                        snapshot.child('Users').child(IdofUser).child('urbees').ref.set(newUrbees);
                        this.setState({ urbees: Math.round(this.state.urbees / 2) });
                    }
                    snapshot.child("appnews").child(lessonId).child("users").child(IdofUser).child("claimed").ref.set('yes');
                    //if the user did not exist in the lesson information, add him and give him the full Urbees 
                } else {
                    snapshot.child("appnews").child(lessonId).child("users").child(IdofUser).child("try").ref.set(0);
                    snapshot.child("appnews").child(lessonId).child("users").child(IdofUser).child("claimed").ref.set('yes');
                    let newUrbees = snapshot.child('Users').child(IdofUser).child('urbees').val() + this.state.urbees;
                    snapshot.child('Users').child(IdofUser).child('urbees').ref.set(newUrbees);
                }
                this.setState({ showQuestionModal: false });
                this.setState({ showCongratsModal: true });
            });
            this.setState({ showCollectButton: false });
        } else {
            //if the user answer incorrectly 
            fire.database().ref().once('value', snapshot => {
                //if the user existed already, add one to the try of the user 
                if (snapshot.child("appnews").child(lessonId).child("users").child(IdofUser).exists()) {
                    let newUserTry = snapshot.child("appnews").child(lessonId).child("users").child(IdofUser).child("try").val() + 1;
                    snapshot.child("appnews").child(lessonId).child("users").child(IdofUser).child("try").ref.set(newUserTry);
                } else {
                    //if the user did not exist, then create it with 1 as value for the tries and claimed as no 
                    snapshot.child("appnews").child(lessonId).child("users").child(IdofUser).child("try").ref.set(1);
                    snapshot.child("appnews").child(lessonId).child("users").child(IdofUser).child("claimed").ref.set('no');
                }
                this.setState({ showQuestionModal: false });
                this.setState({ showWarningModal: true });
            });

        }
    };
    componentDidMount() {
        this.setState(
            {
                lessonId: this.props.navigation.getParam('lessonId', 'no')
            }, () => { // this is for the getPlace function to execute after the setstate is done, otherwise it executes before the place id is set and the database returns null
                this.getLesson();

            });
    }
    render() {
        return (
            <View style={styles.container}>
                <Modal
                    transparent={true}
                    visible={this.state.showQuestionModal}
                >
                    <View style={styles.questionModalBackground}>
                        <View style={styles.modalSeparatorsmall}></View>
                        <View style={styles.questionModal}>
                            <TouchableOpacity style={styles.closeQuestionModal} onPress={() => this.setState({ showQuestionModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.modalTitle}>
                                {this.state.question}
                            </Text>
                            <Text style={styles.modalquestion}>
                                {'1- ' + this.state.answer1}
                            </Text>
                            <Text style={styles.modalquestion}>
                                {'2- ' + this.state.answer2}
                            </Text>
                            <Text style={styles.modalquestion}>
                                {'3- ' + this.state.answer3}
                            </Text>
                            <View style={styles.questionModalButtons}>
                                <Button onPress={() => this.answerQuestion(1)} colorbutton="yellow">
                                    <Text style={styles.modalText}>Answer 1</Text>
                                </Button>
                                <Button onPress={() => this.answerQuestion(2)} colorbutton="yellow">
                                    <Text style={styles.modalText}>Answer 2</Text>
                                </Button>
                                <Button onPress={() => this.answerQuestion(3)} colorbutton="yellow">
                                    <Text style={styles.modalText}>Answer 3</Text>
                                </Button>
                            </View>
                        </View>
                        <View style={styles.modalSeparatorsmall}></View>
                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.showWarningModal}
                >
                    <View style={styles.questionModalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.questionModaltwo}>
                            <TouchableOpacity style={styles.closeQuestionModal} onPress={() => this.setState({ showWarningModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.wariningModalText}>
                                {stringSelector('failed_learning_answer')}
                            </Text>
                            <Button onPress={() => this.setState({ showWarningModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.showCongratsModal}
                >
                    <View style={styles.questionModalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.questionModaltwo}>
                            <TouchableOpacity style={styles.closeQuestionModal} onPress={() => this.setState({ showCongratsModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.modalText}>
                                {stringSelector('congratulations_urbees_erned_event_title1') + " " + this.state.urbees + " " + stringSelector('congratulations_urbees_erned_event_title2')}
                            </Text>
                            <Button onPress={() => this.setState({ showCongratsModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <TouchableOpacity style={styles.back} onPress={() => this.props.navigation.goBack()}>
                    <Ionicons name='ios-arrow-back' size={32} color='black'></Ionicons>
                </TouchableOpacity>
                <ScrollView>
                    <View style={styles.containerTop}>
                        <ImageBackground
                            source={{ uri: this.state.image }}
                            style={styles.imageOfPlace}
                        >
                            <Text style={styles.nameOfPlace}>
                                {this.state.name}
                            </Text>
                        </ImageBackground>
                    </View>
                    <View style={styles.containerBottom}>
                        <View style={styles.webview}>
                            <HTML html={this.state.textUrl} />
                        </View>
                        <View style={styles.twoButtonsAtBottom}>
                            <View style={styles.goButton} visible={this.state.showCollectButton}>
                                <Button colorbutton="yellow" onPress={this.state.showCollectButton ? () => this.askquestion() : () => {/*dont work if the question is already answered */ }}  >{this.state.showCollectButton ? 'Collect Urbees' : 'Urbees Collected'}</Button>
                            </View>
                            <View style={styles.goButton}>
                                <Button colorbutton="orange" onPress={() => this.loadLinkInBrowser()} >Source</Button>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View >
        );
    }

}
const styles = StyleSheet.create({
    container: {
        // justifyContent: "center", //This center vertically when flex-direction is Column
        flex: 1,
        backgroundColor: "#FFFFFF",
    },
    containerTop: {
        flex: 50,

    },

    imageOfPlace: {
        resizeMode: "cover",
        justifyContent: "flex-end",
        flex: 1,
        height: 300,
    },
    containerBottom: {
        flex: 50,
    },
    nameOfPlace: {

        fontSize: 18,
        fontWeight: 'bold',
        color: "white",
        marginLeft: 10,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10

    },
    addressOfPlace: {
        color: "white",
        fontSize: 20,
        marginLeft: 10,
        marginBottom: 3,

    },
    descriptionOfPlace: {
        fontSize: 15,
        textAlign: "center",
        marginHorizontal: 5,
    },
    goButton: {
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 7,
    },
    twoButtonsAtBottom: {
        flexDirection: "column",


    },
    moreInfoButton: {
        width: '30%',
        height: 100,
        alignSelf: 'center',
    },

    locationButton: {
        width: '30%',
        height: 100,
        alignSelf: 'center',

    },
    questionModalBackground: {
        backgroundColor: '#000000aa',
        flex: 1
    },
    questionModal: {
        width: "80%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        justifyContent: "center",
        alignContent: "center",
        alignSelf: "center",
    },
    questionModaltwo: {
        width: "60%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    closeQuestionModal: {
        position: 'absolute',
        top: 30,
        right: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.1)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalTitle: {
        fontSize: 18,
        fontWeight: 'bold',
        width: "80%",
        alignSelf: "center",
    },
    modalquestion: {
        fontSize: 15,
        marginVertical: 10,
        width: "80%",
        alignSelf: "center",
    },
    questionModalButtons: {
        alignItems: 'center',
    },
    modalText: {
        fontSize: 16,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    modalButton: {
        backgroundColor: 'yellow',
        borderRadius: 10,
        marginHorizontal: 10,
        marginBottom: 10,
    },
    wariningModalText: {
        fontSize: 20,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    modalSeparator: {
        flex: 2,
    },
    modalSeparatorsmall: {
        flex: 1,
    },
    back: {
        zIndex:1,
        position: 'absolute',
        top: 10,
        left: 15,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    webview:{
        marginHorizontal:10,
    }
});