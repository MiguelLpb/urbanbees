import React, { useState } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import * as firebase from 'firebase';
import * as Location from 'expo-location';
import { AsyncStorage } from 'react-native';
import stringSelector from '../lenguage/lenguages';
export default class LoadingScreen extends React.Component {
    constructor(props) {
        super(props);
        if(global.language=='english'||global.language=='deutsch'){
            this.state = {
                indexOfSelectedQoute: 0,
                errorMessage:'',
                quotes: [
                    stringSelector('Start_text'),
                    stringSelector('Start_text2'),
                    stringSelector('Start_text3'),
                    stringSelector('Start_text4'),
                    stringSelector('Start_text5'),
                    stringSelector('Start_text6'),
                    stringSelector('Start_text7'),
                    stringSelector('Start_text8'),
                    stringSelector('Start_text9'),
                    stringSelector('Start_text10')]
            };
        }else{
        this.state = {
            indexOfSelectedQoute: 0,
            errorMessage:'',
            quotes: [
                'Get rewarded for saving the planet',
                'Save the planet and become a hero',
                'You can make the difference',
                'We love your enthusiasm!',
                'Set the example for the world',
                'The world needs more people like You',
                'Collect Urbees by helping Earth',
                'We can fight climate change together',
                'Climate action, remastered and fun',
                'Saving the planet can be fun']
        };
    }
    }
    randomSelector() {

        this.setState({ indexOfSelectedQoute: Math.floor(Math.random() * 10) });
    };

    cityLocation = () =>{
        this.setState({ready:false});
        let geoOptions ={
            enableHighAccuracy: true,
            timeOut: 20000,
            maximumAge: 60 * 60 * 24
        };
        navigator.geolocation.getCurrentPosition(this.geoSucces,this.geoFailure,geoOptions);
      }
      geoSucces= (position) =>{
        var positionNow= {
            latitude: position.coords.latitude,
            longitude: position.coords.longitude
        };
        this.currentArea(positionNow);
      }
      currentArea = async(positionNow) => {
        await Location.reverseGeocodeAsync(positionNow).then((res)=>{
            this.setState({cityname:res[0].city});
            global.cityfortheapp=res[0].city;

            AsyncStorage.getItem("language").then((value) => {
                if(value==null){
                    AsyncStorage.setItem("language",'english');
                    global.language='english';
                }else{
                    global.language=value;
                }
            });
            firebase.auth().onAuthStateChanged(user =>{
                this.props.navigation.navigate(user ? "App":"Authentication"); //if the user is authenticated then go to the rest of the app, otherwise go to the Authentication section
            });

        }).catch(err =>{
            this.setState({errorMessage:'There is a problem with the Location'});
            console.log(err);
            firebase.auth().onAuthStateChanged(user =>{
                this.props.navigation.navigate(user ? "App":"Authentication"); //if the user is authenticated then go to the rest of the app, otherwise go to the Authentication section
            });
        });
    }
      geoFailure = (err) => {
        this.setState({errorMessage:'There is a problem with the Location'});
        console.log(err);
        firebase.auth().onAuthStateChanged(user =>{
            this.props.navigation.navigate(user ? "App":"Authentication"); //if the user is authenticated then go to the rest of the app, otherwise go to the Authentication section
        });
     }
    componentDidMount() {
        this.randomSelector();
        setTimeout(() => {  
            this.cityLocation();
        }, 3000);
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.containerTop}>
                    <Image
                        source={require('../../assets/splash.png')}
                        resizeMode='contain'
                        style={styles.logoSplashScreen}
                    ></Image>
                    <Text style={styles.randomText}>{this.state.quotes[this.state.indexOfSelectedQoute]}</Text>
                    <Text style={styles.errorText}>{this.state.errorMessage}</Text>
                </View>
                <View style={styles.containerBottom}>
                    <Text style={styles.byText}>{'by'}</Text>
                    <Text style={styles.appName}>{'UrbanBees'}</Text>
                </View>
            </View>
        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFC71",
    },
    containerTop: {
        flex: 55,
        justifyContent: 'flex-end',
        alignItems: "center",
        marginHorizontal: "5%",

    },
    containerBottom: {
        flex: 45,
        justifyContent: 'flex-end',
        alignItems: "center",
     
    },
    logoSplashScreen: {
        width: '50%',
        height: 200,
    },
    randomText: {
        fontSize: 25,
        fontWeight: "400",
        textAlign: 'center',
    },
    byText: {
        fontSize: 20,
        fontWeight: "400",
    },
    appName: {
        fontSize: 30,
        fontWeight: "400",
        marginBottom: '5%',
    },
    errorText:{
        fontSize: 25,
        fontWeight: "400",
        textAlign: 'center',
        color:'red',
    }


});