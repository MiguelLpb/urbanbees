import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity,ScrollView, Modal } from 'react-native';
import { Input } from '../items/input';
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import fire from '../../config';
import { Button } from '../Button';
import * as firebase from 'firebase';
import stringSelector from '../lenguage/lenguages';
export default class LoginScreen extends React.Component {
    static navigationOptions = {
        headerShown: false
    };
    state = {
        name: '',
        email: '',
        password: '',
        repeatPassword: '',
        authenticating: false,
        errorMessage: null,
        showWarningModal: false,
        ModalMessage: stringSelector("datenschutzenglish"),
    }
    checkIfUserExist=()=>{
        var emailOfUser=this.state.email;
        if(emailOfUser!=''){
            var userIsRegistered=false;
            fire.database().ref('/Users/').once('value', snapshot => {
             snapshot.forEach(function (userToCheck) {
                if(userToCheck.child("useremail").val()==emailOfUser){
                    userIsRegistered=true;
                }
                });
            }).then(()=>{
                if(userIsRegistered){
                    this.setState({errorMessage: "This E-mail is already registered"});
                }else{
                    this.handleSignup();
                }
            }
            );
        }else{
            this.setState({errorMessage: "Please complete the data before register"} );
        }

    };
    handleSignup =()=>{
        //this.setState({showWarningModal:false});
         firebase
            .auth()
            .createUserWithEmailAndPassword(this.state.email, this.state.password)
            .then((res) =>{ 
                fire.database().ref('/Users/'+res.user.uid).set({
                    eventtimes: 0,
                    fristtime: 'yes',
                    firsttimechallenges: 'yes',
                    firsttimenews: 'yes',
                    firsttimeplaces:'yes',
                    firsttimestore:'yes',
                    learningtimes:0,
                    mobilitytimes:0,
                    name: this.state.name,
                    turisticttimes: 0,
                    urbees: 0,
                    useremail:this.state.email,
                })
               return res.user.updateProfile({
                    displayName: this.state.name
                });
            })
            .catch(error =>{this.setState({errorMessage: error.message} ), console.log("error")});
      };
    render() {
        return (
            <View style={styles.container}>
                 <Modal
                    transparent={true}
                    visible={this.state.showWarningModal}
                >
                    <View style={styles.questionModalBackground}>
                        <View style={styles.modalSeparator}></View>

                        <View style={styles.questionModal}>
                            <TouchableOpacity style={styles.closeQuestionModal} onPress={() => this.setState({ showWarningModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <View style={styles.modalSeparator}></View>
                            <View style={styles.wariningModalTextContainer}>
                                <ScrollView>
                                    <View style={styles.wariningModalText}>
                                        <Text >
                                            {this.state.ModalMessage}
                                        </Text>
                                    </View>
                                </ScrollView>
                            </View>
                            <View style={styles.modalButtons}>
                                <View style={styles.modalButtonsinside}>
                                    <Button onPress={() => this.handleSignup()} colorbutton="yellow_small">
                                        <Text style={styles.modalText}>{stringSelector('agree')}</Text>
                                    </Button>
                                    <Button onPress={() => this.setState({ showWarningModal: false })} colorbutton="yellow_small">
                                        <Text style={styles.modalText}>{stringSelector('no')}</Text>
                                    </Button>
                                </View>
                            </View>
                            <View style={styles.modalSeparator}></View>
                        </View>
                        <View style={styles.modalSeparator}></View>

                    </View>
                </Modal>
                <TouchableOpacity style={styles.back} onPress={() => this.props.navigation.goBack()}>
                    <Ionicons name='ios-arrow-back' size={32} color='black'></Ionicons>
                </TouchableOpacity>
                <View style={styles.containerTop}>  
                </View>
                <View style={styles.containerBottom}>
                    <Text style={styles.createAccountText}>Create Account</Text>
                    <Input
                        placeholder='ENTER YOUR NAME'
                        onChangeText={name => this.setState({ name })}
                        value={this.state.name}
                        color={"#ededed" }
                    />
                    <Input
                        placeholder='ENTER YOUR EMAIL'
                        onChangeText={email => this.setState({ email })}
                        value={this.state.email}
                        color={"#ededed" }
                    />
                    <Input
                        placeholder='ENTER PASSWORD'
                        secureTextEntry
                        onChangeText={password => this.setState({ password })}
                        value={this.state.password}
                        color={"#ededed" }
                    />
                    <Input
                        placeholder='REPEAT PASSWORD'
                        secureTextEntry
                        onChangeText={repeatPassword => this.setState({ repeatPassword })}
                        value={this.state.repeatPassword}
                        color={"#ededed" }
                    />
                    <Button 
                    colorbutton="yellow" 
                    onPress={() => this.checkIfUserExist()}
                    >
                        Register
                    </Button>
                    <View style={styles.errorMessage}>
                        {this.state.errorMessage && <Text style={styles.error}>{this.state.errorMessage}</Text>}
                    </View>
                </View>
            </View>
        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",
    },
    containerTop: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    containerBottom: {
        flex: 9,
        alignItems: "center",
        justifyContent: "flex-start",
    },
    logo: {
        width: "100%",
        height: 100,
    },
    appName: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    inputStyle: {
        fontSize: 30,
    },
    registerButton: {
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: 'black',
        width: 220,
        marginBottom: 30,
        marginTop: 30,
    },
    registerButtonText: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold'
    },
    back: {
        zIndex:1,
        position: 'absolute',
        top: 30,
        left: 15,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    errorMessage: {
       height:10,
       justifyContent: "center",
       alignItems: "center",
   },
   error:{
       color: "red",
       fontSize: 20,
       fontWeight: "700", 
       textAlign: "center",
       marginTop: 20,
   },
   createAccountText :{
    color: 'black',
    fontSize: 30,
   },
   questionModalBackground: {
       backgroundColor: '#000000aa',
       flex: 1,
   },
   questionModal: {
       width: "80%",
       borderRadius: 10,
       backgroundColor: '#ffffff',
       flex: 6,
       alignContent: "center",
       alignItems: "center",
       alignSelf: "center",
       display:"flex",
   },
   closeQuestionModal: {
       position: 'absolute',
       top: 30,
       right: 22,
       width: 32,
       height: 32,
       borderRadius: 16,
       backgroundColor: 'rgba(21,22,48,0.1)',
       alignItems: 'center',
       justifyContent: 'center'
   },
   modalTitle: {
       fontSize: 20,
   },
   modalquestion: {
       fontSize: 15,
       marginVertical: 10,
   },
   questionModalButtons: {
       alignItems: 'center',
   },
   modalButtons: {
       flex:2,
   },
   modalButtonsinside:{
       flexDirection:"row",
       justifyContent: 'center',
   },
   wariningModalTextContainer:{
       flex:8,
   },
   modalText: {
       fontSize: 15,
   },
   modalButton: {
       backgroundColor: 'yellow',
       borderRadius: 10,
       marginHorizontal: 10,
       marginBottom: 10,
   },
   wariningModalText: {
       fontSize: 10,
       color: 'black',
       alignSelf: "center",
       width: "80%",
       textAlign: 'justify',
   },
   modalSeparator: {
       flex: 2,
   },
});