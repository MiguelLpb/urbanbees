import React, { useState } from 'react';
import {  StyleSheet, Text, View, TouchableOpacity, LayoutAnimation,FlatList, Image, ImageBackground, EventEmitter  } from 'react-native';
import * as firebase from 'firebase';
import fire from '../../config';
export default class LessonsScreen extends React.Component {
    state ={
        userId:'',
        city: global.cityfortheapp,
        lessons: [],
     };
     ///function to get lessons from firebase
     getLessons= () => {
        let userID=firebase.auth().currentUser.uid;;
        fire.database().ref('/appnews').on('value', snapshot =>{
            var lessonsFromDatabase=[];
            snapshot.forEach(function(lesson) {
                //Check if the lesson was already done by the user
                let claimed="no";
                if(lesson.child('expired').val()=='no'){    
                    if(lesson.child('users').child(userID).exists()){
                        if(lesson.child('users').child(userID).child('claimed').val()=="yes"){
                            claimed="yes"; 
                        }
                    }
                    lessonsFromDatabase.push(
                        {
                            id:lesson.key,
                            name: lesson.child("name").val(),
                            urbees:lesson.child("urbees").val(),
                            image:lesson.child("image").val(),
                            claimed:claimed,
                        }
                    );
                }
            });
            this.setState({lessons:lessonsFromDatabase});
        });
     } 
    componentDidMount(){
        const {userId}= firebase.auth().currentUser.uid;
        this.setState({userId});
        this.getLessons();
    }
    static navigationOptions = {
        headerShown: false
    };  
//This is the design of each part of the scrollview 
      renderLessons= lesson =>{
        return(
          <View style={styles.lesson}>
               <TouchableOpacity style={styles.organizeitems} onPress={()=>this.props.navigation.navigate("Lesson", {lessonId: lesson.id})}>
                    <ImageBackground
                            source={{uri:lesson.image}}
                            style={styles.imageOfLesson}
                        >
                            <View style={ styles.nameTextContainer}>
                                <Text style={styles.nameText}>{lesson.name}</Text>   
                            </View>
                            <View style={ styles.rewardAndClaimedState} >
                            {lesson.claimed=="no" &&
                            <View style={ styles.rewardTextWithImage}>
                                    <Text style={ styles.urbeesText}>{lesson.urbees} </Text>
                                    <Image
                                        source={require('../../assets/urbeecriptocurrency.png')}
                                        resizeMode='contain'
                                        style={styles.urbeeCoin}
                                    ></Image> 
                                </View>}
                                <View style={ styles.checkContainer}>
                                    {lesson.claimed=="yes" &&
                                    <Image
                                        source={require('../../assets/checkbox.png')}
                                        resizeMode='contain'
                                        style={styles.checkImage}
                                    ></Image>}
                                </View>
                            </View>

                    </ImageBackground>
              </TouchableOpacity>
          </View> 

        );
    };

    render(){ 
        //This isfor the design outside of the scroll view
        LayoutAnimation.easeInEaseOut();
        return(
        <View style={styles.container}>
            <View style={styles.topmargin}>
                <FlatList 
                    style={styles.feed}
                    data={this.state.lessons}
                    renderItem={({item})=>this.renderLessons(item)}
                    keyExtractor={item=>item.id}
                    showsVerticalScrollIndicator={false}
                />
            </View>
          </View>
        );
    }  

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF"
      },
      topmargin:{
          marginTop:10,
      },
      lesson:{
       marginBottom: 10,   
       borderColor:"black",
       borderWidth:3, 
       borderRadius: 10,
       flex:1,
       marginHorizontal:20,
      },
      nameText:{
       fontSize:20,
       fontWeight: "700",
       color:"white",
       marginLeft:10,
       marginTop:3,
       textShadowColor: 'rgba(0, 0, 0, 1)',
       textShadowOffset: {width: 1, height: 1},
       textShadowRadius: 3,
      },
      nameTextClaimed:{
        fontSize:18,
        fontWeight: "700",
        color:"white",
        marginLeft:20,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10
       },
      organizeitems:{
       flexDirection: "row",

      },
      urbeesText:{
        fontSize:23,
        fontWeight: "700",
        color:"white",
        marginLeft: 7,
        textShadowColor: 'rgba(0, 0, 0, 1)',
        textShadowOffset: {width: 1, height: 1},
        textShadowRadius: 3,
      },
      dateText:{
        fontSize:18,
        fontWeight: "700",
        color:"white",
        marginLeft:20,
       },
      imageOfLesson: {
        resizeMode: "cover",
        justifyContent: "center",
        overflow: "hidden",
        borderRadius: 5,
        flex:1,
        height:150,
        justifyContent: "space-between",
    },
    rewardAndClaimedState:{
        flexDirection:"row",
        justifyContent:"space-between",
    },
    urbeeCoin:{
        width: 23,
        height: 23,
        alignSelf:"center",
    },
    rewardTextWithImage:{
        flexDirection:"row",
    },
    checkImage:{
        width: 30,
        height: 30,
        alignSelf:"center",
        marginLeft: 10,
        marginBottom: 5,

    },
    checkContainer:{
        alignSelf:"center",
        marginEnd:10,
    }
});