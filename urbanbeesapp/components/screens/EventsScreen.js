import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity,Linking, LayoutAnimation, FlatList, Image, ImageBackground, EventEmitter, Modal } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import * as firebase from 'firebase';
import fire from '../../config';
import { Button } from '../Button';
import stringSelector from '../lenguage/lenguages';
export default class EventsScreen extends React.Component {
    state = {
        userId: '',
        city: global.cityfortheapp,
        events: [],
        showWarningModal: false,
        ModalMessage: stringSelector('apologies'),
        showEvents: true,
    };

    ///function to get events from firebase
    getEvents = () => {
        var reference = fire.database().ref('/Cities/' + this.state.city + '/Events');
        reference.once('value', snapshot => {
            var eventsFromDatabase = [];
            if (snapshot.child('active').val() == 'yes') {
                snapshot.forEach(function (event) {
                    if (event.child('expired').val() == 'no') {
                        //Functionality to check if the date already pass, change the event to expired and not show it in the list
                        let eventDateString = event.child('date').val();
                        let eventDateParts = eventDateString.split(".");
                        let eventDate = new Date(+eventDateParts[2], eventDateParts[1] - 1, +eventDateParts[0]); //This converts our date format from the database to the format for Javascript
                        let today = new Date();
                        today.setHours(0,0,0,0);
                        eventDate.setHours(0,0,0,0);
                        if (today.getTime() <= eventDate.getTime() ) {
                            eventsFromDatabase.push(
                                {
                                    id: event.key,
                                    name: event.child("name").val(),
                                    urbees: event.child("urbees").val(),
                                    date: event.child("date").val(),
                                    image: event.child("image").val(),
                                }
                            );

                        }
                        else {
                            reference.child(event.key).child('expired').set('yes'); 
                        }
                    }
                });
            }
            eventsFromDatabase.sort(function (a, b) { //Function sort with a modification to sort by the closest to the most far date 
                let eventADateParts = a.date.split(".");
                let eventADate = new Date(+eventADateParts[2], eventADateParts[1] - 1, +eventADateParts[0]); //This converts our date format from the database to the format for Javascript
                let eventBDateParts = b.date.split(".");
                let eventBDate = new Date(+eventBDateParts[2], eventBDateParts[1] - 1, +eventBDateParts[0]); //This converts our date format from the database to the format for Javascript
                return eventADate - eventBDate;
            });

            this.setState({ events: eventsFromDatabase });
        }).then(() => {
            if (this.state.events == '') {
                //this.setState({ ModalMessage: stringSelector('apologies') });
                //this.setState({ showWarningModal: true });
                this.setState({ showEvents: false });
            }
        });
    }
    goToPartners = () => {
        Linking.openURL('https://urbanbeespartner.typeform.com/to/VsdPiA');
    }
    componentDidMount() {
        const { userId } = firebase.auth().currentUser.uid;
        this.setState({ userId });
        this.getEvents();
    }

    static navigationOptions = {
        headerShown: false
    };
    //This is the design of each part of the scrollview 
    renderevents = event => {
        return (
            <View style={styles.event}>
                <TouchableOpacity style={styles.organizeitems} onPress={() => this.props.navigation.navigate("Event", { eventId: event.id })}>
                    <ImageBackground
                        source={{ uri: event.image }}
                        style={styles.imageOfEvent}
                    >
                        <View style={styles.eventNameText} >
                            <Text style={styles.nameText}>{event.name}</Text>
                        </View>
                        <View style={styles.rewardAndDateEvent} >
                            <View style={styles.rewardEvent}>
                                <Text style={styles.urbeesText}>{stringSelector("rewardTextChallenges")} </Text>
                                <Image
                                    source={require('../../assets/urbeecriptocurrency.png')}
                                    resizeMode='contain'
                                    style={styles.urbeeCoin}
                                ></Image>
                                <Text style={styles.urbeesText}>{event.urbees} </Text>
                            </View>
                            <View style={styles.eventDateText}>
                                <Text style={styles.dateText}>{event.date} </Text>
                            </View>
                        </View>
                    </ImageBackground>
                </TouchableOpacity>
            </View>

        );
    };

    render() {
        //This isfor the design outside of the scroll view
        LayoutAnimation.easeInEaseOut();
        return (
            <View style={styles.container}>
                <Modal
                    transparent={true}
                    visible={this.state.showWarningModal}
                >
                    <View style={styles.donationModalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.donationModal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ showWarningModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.wariningModalText}>
                                {this.state.ModalMessage}
                            </Text>
                            <Button onPress={() => this.setState({ showWarningModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                {//If there are no events to show, show somethign else
                    this.state.showEvents && (
                        <View style={styles.topmargin}>
                            <FlatList
                                style={styles.feed}
                                data={this.state.events}
                                renderItem={({ item }) => this.renderevents(item)}
                                keyExtractor={item => item.id}
                                showsVerticalScrollIndicator={false}
                            />

                        </View>
                    )}
                {//If there are no events to show, show somethign else
                    !this.state.showEvents && (
                        <View style={styles.noEventsContainer}>

                    <Text style={styles.ohNo}>{stringSelector('NoChallengeTitle')}</Text>
                    <Text style={styles.noEventsText}>{stringSelector('NoEventsText')}</Text>

                            <Text style={styles.callForAction}>{stringSelector('NoEventsCallForAction')}</Text>
                            <Button
                                onPress={() => this.goToPartners()} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('contact_button')}</Text>
                            </Button>

                        </View>
                    )}
            </View>
        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF"
    },
    topmargin: {
        marginTop: 10,
    },
    event: {
        marginBottom: 10,
        borderColor: "black",
        borderWidth: 3,
        borderRadius: 10,
        flex: 1,
        marginHorizontal: 20,
    },
    nameText: {
        flexWrap: 'wrap',
        fontSize:20,
       fontWeight: "700",
       color:"white",
       marginLeft:10,
       marginTop:3,
       textShadowColor: 'rgba(0, 0, 0, 1)',
       textShadowOffset: {width: 1, height: 1},
       textShadowRadius: 3,
    },
    eventNameText: {

    },
    organizeitems: {
        flexDirection: "row",

    },
    urbeesText: {
        fontSize: 18,
        fontWeight: "700",
        color: "white",
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10
    },
    dateText: {
        fontSize: 18,
        fontWeight: "700",
        color: "white",
        marginLeft: 10,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10
    },
    imageOfEvent: {
        resizeMode: "cover",
        justifyContent: "space-between",
        overflow: "hidden",
        borderRadius: 5,
        flex: 1,
        height: 150,
        flexDirection: "column-reverse",

    },
    donationModalBackground: {
        backgroundColor: '#000000aa',
        flex: 1
    },
    donationModal: {
        width: "80%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    modalTitle: {
        fontSize: 30,
    },
    donationModalButtons: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    modalButton: {
        backgroundColor: 'yellow',
        borderRadius: 10,
        marginHorizontal: 10,
    },
    modalText: {
        fontSize: 15,
    },
    closeModal: {
        position: 'absolute',
        top: 30,
        right: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.1)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    wariningModalText: {
        fontSize: 14,
        color: "black",
        fontWeight: "bold",
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    urbeeCoin: {
        width: 18,
        height: 18,
        alignSelf: "center",
    },
    rewardEvent: {
        flexDirection: "row",
        justifyContent: "center",
    },
    eventDateText: {

    },
    rewardAndDateEvent: {
        flexDirection: "row-reverse",
        justifyContent: "space-between",
    },
    noEventsContainer: {
        alignItems: 'center',
        fontSize: 20,
        textAlign: 'justify',
        //flex: 1,
    },
    noEventsText: {
        width: '80%',
        alignItems: 'center',
        fontSize: 20,
        textAlign: 'justify',
    },
    ohNo: {
        fontWeight: 'bold',
        fontSize: 40,
        textAlign: 'center',
    },
    callForAction: {
        width: '80%',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'justify',
        marginTop: '7%',
    },
    ohNoAndTextContainer: {
        // flex: 1,
    },
    callForActionAndButton: {
        //flex: 1,
    },
    modalSeparator: {
        flex: 2,
    },
});