import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, LayoutAnimation } from 'react-native';
import { Input } from '../items/input';
import {Ionicons} from '@expo/vector-icons';
import { Button } from '../Button';
import * as firebase from 'firebase';

export default class LoginScreen extends React.Component {
    static navigationOptions = {
        headerShown: false
    };  
    state= {
        email: '',
        password: '',
        authenticating: false,
        errorMessage: null
      }
      handleLogin =()=>{
        const {email, password}= this.state;
        firebase
          .auth()
          .signInWithEmailAndPassword(email,password)
          .catch(error=> this.setState({errorMessage: error.message}));
    };
    render(){ 
        LayoutAnimation.easeInEaseOut(); //adds cool animation to the opening of the screen (had to import LayoutAnimation for this, but nothing needed to be installed)
        return(
            <View style={styles.container}>
                <TouchableOpacity  style={styles.back} onPress={() => this.props.navigation.goBack()}>
                    <Ionicons name='ios-arrow-back' size={32} color='black'></Ionicons>
                </TouchableOpacity>
                <View style={styles.containerTop}>

                </View>
                <View style={styles.containerBottom}>
                    <Text style={styles.welcomeText}>Welcome back</Text>
                    <Input
                        placeholder= 'ENTER EMAIL'
                        onChangeText= {email => this.setState({ email })}
                        value={this.state.email}
                        color={"#ededed" }
                    />
                    <Input
                        placeholder= 'ENTER PASSWORD'
                        secureTextEntry
                        onChangeText= {password => this.setState({ password })}
                        value={this.state.password}   
                        color={"#ededed" }                 
                    />
                    <Button 
                    colorbutton="yellow" 
                    onPress={() => this.handleLogin()}
                    >
                        Login
                    </Button>
                    <View style={styles.errorMessage}>
                        {this.state.errorMessage && <Text style={styles.error}>{this.state.errorMessage}</Text>}
                    </View>
                </View>
            </View>
        );
    }  

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white",

    },
    containerTop: {
        flex: 1,
        alignItems:"center",
        justifyContent:"flex-start",
    },
    containerBottom: {
        flex: 9,
        alignItems:"center",
        justifyContent:"flex-start",

    }, 
   logo: {
        width: "100%",
        height: 100,
   },
   appName: {
    fontSize: 20,
    fontWeight: 'bold'
 },
 loginButton: {
    borderStyle: 'solid',
    borderWidth: 2,
    borderColor: 'black',
    width: 220,
    marginBottom: 20,
    marginTop: 30,
    
},
loginButtonText: {
    textAlign: 'center',
    fontSize: 20,
    fontWeight:'bold'
 },
 back: {
     zIndex:1,
     position: 'absolute',
     top: 30,
     left: 15,
     width: 32,
     height: 32,
     borderRadius: 16,
     backgroundColor: 'rgba(21,22,48,0)',
     alignItems: 'center',
     justifyContent: 'center'
 },
 errorMessage: {
    height:72,
    justifyContent: "center",
    alignItems: "center",
},
error:{
    color: "red",
    fontSize: 20,
    fontWeight: "700", 
    textAlign: "center"
},
welcomeText:{
    color: 'black',
    fontSize: 30,
}
});