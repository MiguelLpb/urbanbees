import React, { useState } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, LayoutAnimation, FlatList, Image, ImageBackground, Modal, } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import * as firebase from 'firebase';
import { getDistance } from 'geolib';
import { Button } from '../Button';
import fire from '../../config';
import stringSelector from '../lenguage/lenguages';
export default class PlacesScreen extends React.Component {
    state = {
        userId: '',
        city: global.cityfortheapp,
        places: [],
        placesOrdered: [],
        showWarningModal: false,
        ModalMessage: stringSelector('apologies'),
        showplaces: true,
        ready: true,
        userlat: 0,
        userlon: 0,
    };
    ///function to get Places from firebase
    getPlaces = () => {
        fire.database().ref('/Cities/' + this.state.city + '/Tplaces').once('value', snapshot => {
            var placesFromDatabase = [];
            if (snapshot.child('active').val() == 'yes') {
                snapshot.forEach(function (place) {
                    if (place.child('active').val() == 'yes') {
                        placesFromDatabase.push(
                            {
                                id: place.key,
                                name: place.child("name").val(),
                                urbees: place.child("urbees").val(),
                                image: place.child("image").val(),
                                lat: place.child('lat').val(),
                                lon: place.child('lon').val(),
                            }
                        );
                    }
                });
            }else{
                this.setState({ showplaces: false });
            }
            this.setState({ places: placesFromDatabase });
        }).then(() => {
            if (this.state.places == '') {
                this.setState({ ModalMessage:stringSelector('apologies')});
                this.setState({showWarningModal:true});
                this.setState({ showplaces: false });
            }else{
                this.getUserLocation();
            }
        });
    }

    //Function that with the lat and lon of the place checks if the user is in the same place as this one
    getUserLocation = () => {
        this.setState({ ready: false });
        let geoOptions = {
            enableHighAccuracy: true,
            timeOut: 20000,
            maximumAge: 60 * 60 * 24
        };
        navigator.geolocation.getCurrentPosition(this.geoSucces, this.geoFailure, geoOptions);
    }
    geoSucces = (position) => {
        this.setState({ userlat: position.coords.latitude });
        this.setState({ userlon: position.coords.longitude });
        this.organizePlacesByLocation();
    }
    geoFailure = (err) => {
        console.log("Error getting location "+err);
        this.setState({ ready: true });
        this.setState({ placesOrdered: this.state.places });  
    }
    organizePlacesByLocation= ()=>{
        var placesToOrganize= this.state.places;
        var userLat=this.state.userlat;
        var userLon=this.state.userlon;
        placesToOrganize.sort(function(a,b){
            var distanceToPlaceA = getDistance(
                { latitude: a.lat, longitude: a.lon },
                { latitude: userLat, longitude: userLon }
            );
            var distanceToPlaceB = getDistance(
                { latitude: b.lat, longitude: b.lon },
                { latitude: userLat, longitude: userLon }
            );
            return distanceToPlaceA - distanceToPlaceB;
        });
        this.setState({ placesOrdered: placesToOrganize });  
    };
    componentDidMount() {
        const { userId } = firebase.auth().currentUser.uid;
        this.setState({ userId });
        this.getPlaces();
        
    }
    static navigationOptions = {
        headerShown: false
    };
    //This is the design of each part of the scrollview 
    renderPlaces = specificPlace => {
        return (
            <View style={styles.place}>
                <TouchableOpacity style={styles.organizeitems} onPress={() => this.props.navigation.navigate("Place", { placeId: specificPlace.id })}>
                    <ImageBackground
                        source={{ uri: specificPlace.image }}
                        //resizeMode='contain'
                        style={styles.imageOfPlace}
                    >
                        <View style={styles.rewardPlace}>
                            <Text style={styles.urbeesText}>{stringSelector("rewardTextChallenges")} </Text>
                            <Image
                                source={require('../../assets/urbeecriptocurrency.png')}
                                resizeMode='contain'
                                style={styles.urbeeCoin}
                            ></Image>
                            <Text style={styles.urbeesText}>{specificPlace.urbees} </Text>
                        </View>
                        <View style={styles.namePlace}>
                            <Text style={styles.nameText}>{specificPlace.name}</Text>
                        </View>
                    </ImageBackground>
                </TouchableOpacity>
            </View>

        );
    };

    render() {
        //This is for the design outside of the scroll view
        LayoutAnimation.easeInEaseOut();
        return (
            <View style={styles.container}>
                <Modal
                    transparent={true}
                    visible={this.state.showWarningModal}
                >
                    <View style={styles.donationModalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.donationModal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ showWarningModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.wariningModalText}>
                                {this.state.ModalMessage}
                            </Text>
                            <Button onPress={() => this.setState({ showWarningModal: false })} colorbutton="yellow_small"> 
                            <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                             </Button> 
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                {//If there are no events to show, show somethign else
                    this.state.showplaces && (
                        <View style={styles.topmargin}>
                            <FlatList
                                style={styles.feed}
                                data={this.state.placesOrdered}
                                renderItem={({ item }) => this.renderPlaces(item)}
                                keyExtractor={item => item.id}
                                showsVerticalScrollIndicator={false}
                            />
                        </View>
                    )}
                {//If there are no events to show, show somethign else
                    !this.state.showplaces && (
                        <View style={styles.noPlacesContainer}>
                            <Text style={styles.ohNo}>{stringSelector('NoChallengeTitle')}</Text>
                            <Text style={styles.noPlacesText}>{stringSelector('NoPlacesText')}</Text>
                            <Text style={styles.callForAction}>{stringSelector('NoPlacesCallForAction')}</Text>
                        </View>
                    )}
            </View>
        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF"
    },
    topmargin: {
        marginTop: 10,
    },
    place: {
        marginBottom: 10,
        borderColor: "black",
        borderWidth: 3,
        borderRadius: 10,
        flex: 1,
        marginHorizontal: 20,
    },
    nameText: {
        fontSize:20,
        fontWeight: "700",
        color:"white",
        marginLeft:10,
        marginTop:3,
        textShadowColor: 'rgba(0, 0, 0, 1)',
        textShadowOffset: {width: 1, height: 1},
        textShadowRadius: 3,
    },
    organizeitems: {
        flexDirection: "row",
    },
    urbeesText: {
        fontSize:21,
       fontWeight: "700",
       color:"white",
       marginLeft:10,
       textShadowColor: 'rgba(0, 0, 0, 1)',
       textShadowOffset: {width: 1, height: 1},
       textShadowRadius: 3,
    },
    dateText: {
        fontSize: 20,
        fontWeight: "700",
        color: "white",
        marginLeft: 20,
    },
    imageOfPlace: {
        resizeMode: "cover",
        justifyContent: "space-between",
        overflow: "hidden",
        borderRadius: 5,
        flex: 1,
        height: 150,
    },
    donationModalBackground: {
        backgroundColor: '#000000aa',
        flex: 1
    },
    donationModal: {
        width: "80%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    modalTitle: {
        fontSize: 30,
    },
    donationModalButtons: {
        flexDirection: 'row',
        justifyContent: 'center',
    },
    modalButton: {
        backgroundColor: 'yellow',
        borderRadius: 10,
        marginHorizontal: 10,
    },
    modalText: {
        fontSize: 15,
    },
    closeModal: {
        position: 'absolute',
        top: 30,
        right: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.1)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    wariningModalText: {
        fontSize: 15,
        color: 'black',
        width:"80%",
    },
    rewardPlace: {
        alignSelf: "flex-end",
        flexDirection: "row",

    },
    namePlace: {
        alignSelf: "flex-start",
    },
    urbeeCoin: {
        width: 20,
        height: 20,
        marginHorizontal: -5,
        alignSelf: "center",
    },
    noPlacesContainer: {
        alignItems: 'center',
        fontSize: 20,
        textAlign: 'justify',
        //flex: 1,
    },
    ohNo: {
        fontWeight: 'bold',
        fontSize: 40,
        textAlign: 'center',
    },
    noPlacesText: {
        width: '80%',
        alignItems: 'center',
        fontSize: 20,
        textAlign: 'justify',
    },
    callForAction: {
        width: '80%',
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'justify',
        marginTop: '7%',

    },
    modalSeparator:{
        flex: 3,
    }
});