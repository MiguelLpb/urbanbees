import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Modal, ImageBackground, Linking } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import * as firebase from 'firebase';
import fire from '../../config';
import { Button } from '../Button';
import { Input } from '../items/input';
import { Updates } from 'expo';
import { AsyncStorage } from 'react-native';
import stringSelector from '../lenguage/lenguages';
export default class ProfileScreen extends React.Component {
    static navigationOptions = {
        headerShown: false,
    };

    signOutUser = () => {
        firebase.auth().signOut();
    };
    beeInfo = (bee) => {
        var message = '';
        if (bee == 1) {
            message = stringSelector('FitBee') + ': ' + stringSelector('FitBeeDescription');
        } else if (bee == 2) {

            message = stringSelector('ExplorerBee') + ': ' + stringSelector('ExplorerBeeDescription');
        }
        else if (bee == 3) {
            message = stringSelector('polinizerBee') + ': ' + stringSelector('polinizerBeeDescription');
        }
        else if (bee == 4) {
            message = stringSelector('LearningBee') + ': ' + stringSelector('LearningBeeDescription');
        }
        this.setState({
            showWarningModal: true,
            ModalMessage: message
        });

    }
    state = {
        userName: 'Loading name..',
        urbees: 0,
        fitBeeTimes: 0,
        explorerBreeTimes: 0,
        pollinatingBeeTimes: 0,
        learningBeeTimes: 0,
        fitBeeImage: require('../../assets/bees/fit1.png'),
        explorerBreeImage: require('../../assets/bees/explorer1.png'),
        pollinatingBeeImage: require('../../assets/bees/pollinating1.png'),
        learningBeeImage: require('../../assets/bees/learning1.png'),
        showWarningModal: false,
        ModalMessage: '',
        ShowClaimingModal: false,
        claimCode: '',
    }
    checkCode = () => {
        var userId = firebase.auth().currentUser.uid;
        var codeClaimed = false;
        var claimCode = this.state.claimCode;
        var codeUsed = false;
        fire.database().ref('/codes').once('value', snapshot => {
            snapshot.forEach(function (codeToClaim) {
                if (claimCode == codeToClaim.key) {
                    if (codeToClaim.child('claimed').val() == 'no') {
                        codeClaimed = true;
                        codeToClaim.child('claimed').ref.set('yes');
                        fire.database().ref('/Users/' + userId).once('value', snapshot => {
                            var newUrbees = snapshot.child('urbees').val() + 420;
                            snapshot.child('urbees').ref.set(newUrbees);
                        });
                    } else {
                        codeUsed = true;

                    }
                }
            });
        }).then(() => {
            this.setState({ ShowClaimingModal: false });
            if (codeUsed) {
                this.setState({ ShowClaimingModal: false });
                this.setState({
                    showWarningModal: true,
                    ModalMessage: stringSelector('EarnedbyclaimingCodeUsed'),
                });
            } else {
                if (codeClaimed == true) {
                    this.setState({
                        showWarningModal: true,
                        ModalMessage: stringSelector('EarnedbyclaimingCode'),
                    });
                } else {
                    this.setState({
                        showWarningModal: true,
                        ModalMessage: stringSelector('EarnedbyclaimingCodeWrong'),
                    });
                }
            }
        });
    };
    changeLenguage = () => {
        if (global.language == 'english') {
            AsyncStorage.setItem("language", 'deutsch').then(() => {
                Updates.reload();
            });
        } else {
            AsyncStorage.setItem("language", 'english').then(() => {
                Updates.reload();
            });
        }

    }
    componentDidMount() {
        var userNameFromfirebase = (JSON.stringify(firebase.auth().currentUser.displayName) == 'null' )?stringSelector('appleprofile'):firebase.auth().currentUser.displayName;
        this.setState({ userName: userNameFromfirebase });
        var userId = firebase.auth().currentUser.uid;
        fire.database().ref('/Users/' + userId).on('value', snapshot => {
            let urbeesbag = snapshot.child('urbees').val();
            this.setState({ urbees: urbeesbag });
            let fitBeeAmount = snapshot.child('mobilitytimes').val();
            let explorerBreeAmount = snapshot.child('turisticttimes').val();
            let pollinatingBeeAmount = snapshot.child('eventtimes').val();
            let learningBeeTimes = snapshot.child('learningtimes').val();
            this.setState({ fitBeeTimes: fitBeeAmount });
            this.setState({ explorerBreeTimes: explorerBreeAmount });
            this.setState({ pollinatingBeeTimes: pollinatingBeeAmount });
            this.setState({ learningBeeTimes: learningBeeTimes });
            //Change images of bees depending on the level 
            if (fitBeeAmount > 200 && fitBeeAmount < 1000) {
                this.setState({ fitBeeImage: require('../../assets/bees/fit2.png') });
            } else if (fitBeeAmount >= 1000) {
                this.setState({ fitBeeImage: require('../../assets/bees/fit3.png') });
            }
            if (explorerBreeAmount > 20 && explorerBreeAmount < 50) {
                this.setState({ explorerBreeImage: require('../../assets/bees/explorer2.png') });
            } else if (explorerBreeAmount >= 50) {
                this.setState({ explorerBreeImage: require('../../assets/bees/explorer3.png') });
            }
            if (pollinatingBeeAmount > 10 && pollinatingBeeAmount < 30) {
                this.setState({ pollinatingBeeImage: require('../../assets/bees/pollinating2.png') });
            } else if (pollinatingBeeAmount >= 30) {
                this.setState({ pollinatingBeeImage: require('../../assets/bees/pollinating3.png') });
            }
            if (learningBeeTimes > 10 && learningBeeTimes < 50) {
                this.setState({ learningBeeImage: require('../../assets/bees/learning2.png') });
            } else if (learningBeeTimes >= 50) {
                this.setState({ learningBeeImage: require('../../assets/bees/learning3.png') });
            }

        });


    }
    render() {
        const gradientHeight = 500;
        const gradientBackground = "#f6c108";
        const data = Array.from({ lenght: gradientHeight });
        return (
            <View style={styles.container}>
                <Modal
                    transparent={true}
                    visible={this.state.showWarningModal}
                >
                    <View style={styles.questionModalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.questionModalBees}>
                            <TouchableOpacity style={styles.closeQuestionModal} onPress={() => this.setState({ showWarningModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.wariningModalText}>
                                {this.state.ModalMessage}
                            </Text>
                            <Button onPress={() => this.setState({ showWarningModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.ShowClaimingModal}
                >
                    <View style={styles.questionModalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.questionModal}>
                            <TouchableOpacity style={styles.closeQuestionModal} onPress={() => this.setState({ ShowClaimingModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.wariningModalText}>
                                {stringSelector('Codes_claim_explained')}
                            </Text>
                            <View style={styles.inputButtonModalFormat}>
                                <Input
                                    placeholder='ENTER CODE'
                                    onChangeText={claimCode => this.setState({ claimCode })}
                                    value={this.state.claimCode}
                                    color={"#ededed" }
                                />
                                <Button onPress={() => { this.checkCode() }} colorbutton="yellow" >{stringSelector('claimUrbees')}</Button>
                            </View>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <ImageBackground
                    source={require('../../assets/backgrounds/beehiveInsideBackground.png')}
                    style={styles.beehiveInsideBackground}
                >
                <View style={styles.containerTop}>
                    <View style={styles.Separator}></View>
                    <View style={styles.containerTitle}>
                        <View style={styles.modalSeparator}></View>
                        <Text style={styles.userNameProfile}>
                            {this.state.userName}
                        </Text>
                        <TouchableOpacity style={styles.datenschutzButton} onPress={() => {
                        Linking.openURL("https://urbanbeesapp.com/datenschutz.html").catch(err => console.error("Couldn't load page", err));
                    }}>
                        <ImageBackground
                            source={require("../../assets/datenschutzIcon.png")}
                            style={styles.datenschutzIcon}
                        >
                        </ImageBackground>
                    </TouchableOpacity>
                    </View>
                    <View style={styles.containerMIddleAdjustment}>
                        <Image
                            source={require('../../assets/urbeecriptocurrency.png')}
                            resizeMode='contain'
                            style={styles.urbeeCoin}
                        ></Image>
                        <Text style={styles.urbeesNumber}>
                            {this.state.urbees}
                        </Text>
                    </View>
                </View>
                <View style={styles.containerBottom}>
                    <View style={styles.containerMIddleAdjustment}>
                        <TouchableOpacity style={styles.containerBee} onPress={() => { this.beeInfo(1) }} >
                            <Text style={styles.beeName}>
                                {this.state.fitBeeTimes}
                            </Text>
                            <View style={styles.circle}>
                                <Image
                                    source={this.state.fitBeeImage}
                                    resizeMode='contain'
                                    style={styles.beeImage}
                                ></Image>
                            </View>
                            <Text style={styles.beeName}>
                                {stringSelector('FitBee')}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.containerBee} onPress={() => { this.beeInfo(2) }}>
                            <Text style={styles.beeName}>
                                {this.state.explorerBreeTimes}
                            </Text>
                            <View style={styles.circle}>
                                <Image
                                    source={this.state.explorerBreeImage}
                                    resizeMode='contain'
                                    style={styles.beeImage}
                                ></Image>
                            </View>
                            <Text style={styles.beeName}>
                                {stringSelector('ExplorerBee')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={styles.containerBottom}>
                    <View style={styles.containerMIddleAdjustment}>
                        <TouchableOpacity style={styles.containerBee} onPress={() => { this.beeInfo(3) }}>
                            <Text style={styles.beeName}>
                                {this.state.pollinatingBeeTimes}
                            </Text>
                            <View style={styles.circle}>
                                <Image
                                    source={this.state.pollinatingBeeImage}
                                    resizeMode='contain'
                                    style={styles.beeImage}
                                ></Image>
                            </View>
                            <Text style={styles.beeName}>
                                {stringSelector('polinizerBee')}
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.containerBee} onPress={() => { this.beeInfo(4) }}>
                            <Text style={styles.beeName}>
                                {this.state.learningBeeTimes}
                            </Text>
                            <View style={styles.circle}>
                                <Image
                                    source={this.state.learningBeeImage}
                                    resizeMode='contain'
                                    style={styles.beeImage}
                                ></Image>
                            </View>
                            <Text style={styles.beeName}>
                                {stringSelector('LearningBee')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={styles.containerBottom}>
                    <View style={styles.lenguageAndLogoutButton}>
                    <Button onPress={() => this.setState({ ShowClaimingModal: true })} colorbutton="yellow" >{stringSelector('Codes_caim')}</Button>
                    </View>
                    <View style={styles.lenguageAndLogoutButton}>
                        <Button onPress={() => this.changeLenguage()} colorbutton="light_orange_small" >{stringSelector('language_button')}</Button>
                        <Text style="separator"> </Text>
                        <Button onPress={this.signOutUser} colorbutton="light_orange_small" >{stringSelector('logout')}</Button>
                    </View>
                </View>
                </ImageBackground>
            </View >
        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
 
    },
    containerTop: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",

    },
    containerBottom: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",

    },
    containerBottom2: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        

    },
    containerMIddleAdjustment: {
        flexDirection: "row",
        alignItems: "center",

    },
    urbeeCoin: {
        width: 30,
        height: 30,

    },
    urbeesNumber: {
        fontSize: 30,
        color: 'white',

    },
    containerBee: {
        flex: 5,
        alignItems: "center",
        flexDirection: "column",
    },
    beeName: {
        fontSize: 15,
        fontWeight: "bold",
        color: 'white',
    },
    beeImage: {
        width: '100%',
        height: 70,
    },
    userNameProfile: {
        fontSize: 40,
        color: 'white',
        flex: 5,

    },
    containerTitle: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    datenschutzButton: {
        flex: 1,

    },
    datenschutzIcon: {
        width: 40,
        height: 40,
        
    },
    questionModalBackground: {
        backgroundColor: '#000000aa',
        flex: 1,
    },
    questionModal: {
        width: "80%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        justifyContent: "center",
        alignContent: "center",
        alignSelf: "center",
    },
    modalSeparator: {
        flex: 2,
    },
    questionModalBees: {
        width: "80%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    closeQuestionModal: {
        position: 'absolute',
        top: 30,
        right: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.1)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalTitle: {
        fontSize: 20,
    },
    modalquestion: {
        fontSize: 15,
        marginVertical: 10,
    },
    questionModalButtons: {
        alignItems: 'center',
    },
    modalText: {
        fontSize: 16,
    },
    modalButton: {
        backgroundColor: 'yellow',
        borderRadius: 10,
        marginHorizontal: 10,
        marginBottom: 10,
    },
    wariningModalText: {
        fontSize: 20,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    lenguageAndLogoutButton: {
        flexDirection: "row",
    },
    separator: {
        marginVertical: 10,
    },
    circle: {
        width: 80,
        height: 80,
        backgroundColor: "#8d4b0f",
        borderRadius: 40,
        borderWidth: 5,
        borderColor: "#592d05"
    },
    inputButtonModalFormat: {
        alignItems: "center",
    },
    beehiveInsideBackground: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

