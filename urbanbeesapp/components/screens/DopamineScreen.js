import React, { useState } from 'react';
import {  ImageBackground } from 'react-native';
import { StyleSheet, Text, View, Image, Modal, TouchableOpacity, ActivityIndicator,ScrollView } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import * as TaskManager from 'expo-task-manager';
import * as firebase from 'firebase';
import { Button } from '../Button';
import fire from '../../config';
import { Pedometer } from 'expo-sensors';
import { AsyncStorage } from 'react-native';
import stringSelector from '../lenguage/lenguages';
export default class DopamineScreen extends React.Component {

    state = {
        cityname: global.cityfortheapp,
        urbees: 0,
        showWarningModal: false,
        ready: true,
        ModalMessage: '',
        //For walking recognition
        isPedometerAvailable: 'checking',
        pastStepCount: 0,
        currentStepCount: 0,
        totalsteps: 0,
        urbeesToGetForWalking: 0,
        ImageBeeHive: require('../../assets/beehives/dopaminBeehiveStage0.png'),
        ready: true,
        showWarningModalDaten: false,
        ModalMessageDaten: stringSelector("datenschutzenglish"),
    };

    componentDidMount() {
        TaskManager.unregisterAllTasksAsync();
        var userId = firebase.auth().currentUser.uid;
        fire.database().ref('/Users/' + userId).on('value', snapshot => {
            let urbeesbag = snapshot.child('urbees').val();
            this.setState({ urbees: urbeesbag });
            let firstttimeUser = snapshot.child('fristtime').val();
            if (firstttimeUser == 'yes') {
                this.setState({ ModalMessage: stringSelector('welcome_text') });
                this.setState({ showWarningModalDaten: true });
            }
        });
        this._subscribe();
        //context=this;
        AsyncStorage.getItem("stepsUser").then((value) => {
            var valueofoldsteps=   value;
            if(value=="NaN"){
                valueofoldsteps="0";
            }
            this.setState({ totalsteps: parseInt(valueofoldsteps, 10) });
            console.log("steps: "+parseInt(valueofoldsteps, 10));
        }).then(() => {
            this._subscribe();
            if (this.state.totalsteps > 166) {
                this.setState({ urbeesToGetForWalking: Math.round(this.state.totalsteps / 166) });// Every 166 steps is 1 urbee, so every 1000 steps is 6 urbees and every 10.000 (avarage steps a person does a day) is 60 urbees, so in one week a person can make 420 urbees by walking. 
            }
        }).then(() => {
            this.changeBeeHiveImage();
        });
    }
    componentWillUnmount() {

        this._subscription && this._subscription.remove();
        this._unsubscribe();
    }

    _subscribe = () => {
        Pedometer.isAvailableAsync().then(
            result => {
                this.setState({
                    isPedometerAvailable: String(result),
                });
                if (result == true) {
                    this._subscription = Pedometer.watchStepCount(result => {
                        let newStepsUserCount = parseInt(this.state.totalsteps, 10) + (result.steps - this.state.currentStepCount);
                        AsyncStorage.setItem("stepsUser", (newStepsUserCount).toString());
                        this.setState({
                            currentStepCount: result.steps,
                            totalsteps: newStepsUserCount,
                        });
                        if (this.state.totalsteps > 166) {
                            this.setState({ urbeesToGetForWalking: Math.round(this.state.totalsteps / 166) });// Every 166 steps is 1 urbee, so every 1000 steps is 6 urbees and every 10.000 (avarage steps a person does a day) is 60 urbees, so in one week a person can make 420 urbees by walking. 
                            this.changeBeeHiveImage();
                        }
                    });
                }
            },
            error => {
                this.setState({
                    isPedometerAvailable: 'Could not get isPedometerAvailable: ' + error,
                });
            }
        );
    };

    _unsubscribe = () => {
        this._subscription && this._subscription.remove();
        this._subscription = null;
    };

    getUrbeesForWalking = () => {
      
        var userId = firebase.auth().currentUser.uid;
        let UrbeesToClaim = this.state.urbeesToGetForWalking;
        if (UrbeesToClaim >= 1) {
            this.setState({ ready: false });
            fire.database().ref('/Users/' + userId).once('value', snapshot => {
                let newUrbeesCount = snapshot.child('urbees').val() + UrbeesToClaim;
                let newMobilitytimesAmount = snapshot.child('mobilitytimes').val() + 1;
                snapshot.child('urbees').ref.set(newUrbeesCount);
                snapshot.child('mobilitytimes').ref.set(newMobilitytimesAmount);
            }).then(() => {
                AsyncStorage.setItem("stepsUser", '0').then(()=>{
                    this.setState({
                        currentStepCount: 0,
                        totalsteps: 0,
                        pastStepCount: 0,
                        urbeesToGetForWalking: 0,
                    }, ()=> {
                        this.changeBeeHiveImage();
                    } );
                 });
            }).then(() => {
                this.setState({ ready: true });
                this.setState({ ModalMessage: stringSelector('congratulations_urbees_erned_event_title1') + UrbeesToClaim + stringSelector('congratulations_urbees_erned_dopamine_title2') + stringSelector('congratulations_urbees_erned_dopamine_text') });
                this.setState({ showWarningModal: true });
            });
        } else {
            this.setState({ ModalMessage: stringSelector('dopamine_no_urbees_title') });
            this.setState({ showWarningModal: true });
        }
    };
    changeBeeHiveImage = () => {
        let urbees_total = this.state.urbeesToGetForWalking;
        if (urbees_total == 0) {
            this.setState({ ImageBeeHive: require('../../assets/beehives/dopaminBeehiveStage0.png') });
        }
        else if (urbees_total < 10) {
            this.setState({ ImageBeeHive: require('../../assets/beehives/dopaminBeehiveStage1.png') });
        } else if (urbees_total < 20) {
            this.setState({ ImageBeeHive: require('../../assets/beehives/dopaminBeehiveStage2.png') });
        } else if (urbees_total < 30) {
            this.setState({ ImageBeeHive: require('../../assets/beehives/dopaminBeehiveStage3.png') });
        } else if (urbees_total < 40) {
            this.setState({ ImageBeeHive: require('../../assets/beehives/dopaminBeehiveStage4.png') });
        } else if (urbees_total < 50) {
            this.setState({ ImageBeeHive: require('../../assets/beehives/dopaminBeehiveStage5.png') });
        } else if (urbees_total < 60) {
            this.setState({ ImageBeeHive: require('../../assets/beehives/dopaminBeehiveStage6.png') });
        } else if (urbees_total < 70) {
            this.setState({ ImageBeeHive: require('../../assets/beehives/dopaminBeehiveStage7.png') });
        } else if (urbees_total < 80) {
            this.setState({ ImageBeeHive: require('../../assets/beehives/dopaminBeehiveStage8.png') });
        } else if (urbees_total < 90) {
            this.setState({ ImageBeeHive: require('../../assets/beehives/dopaminBeehiveStage9.png') });
        } else if (urbees_total < 100000) {
            this.setState({ ImageBeeHive: require('../../assets/beehives/dopaminBeehiveStage10.png') });
        }
    }
    handleDatenSchutz=(desition)=>{
        if(desition==0){
            var userId = firebase.auth().currentUser.uid;
            fire.database().ref('/Users/' + userId).on('value', snapshot => {
                snapshot.child('fristtime').ref.set('no');
                this.setState({ showWarningModalDaten: false });
                this.setState({ showWarningModal: true });     
            });
        }else{
            this.setState({ showWarningModalDaten: false });
            firebase.auth().signOut();
        }
    }
    render() {
        return (
            <View style={styles.container}>
                 <Modal
                    transparent={true}
                    visible={this.state.showWarningModalDaten}
                >
                    <View style={styles.questionModalBackground1}>
                        <View style={styles.modalSeparator1}></View>

                        <View style={styles.questionModal1}>
                            <TouchableOpacity style={styles.closeQuestionModal1} onPress={() => this.setState({ showWarningModalDaten: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <View style={styles.modalSeparator1}></View>
                            <View style={styles.wariningModalTextContainer1}>
                                <ScrollView>
                                    <View style={styles.wariningModalText1}>
                                        <Text >
                                            {this.state.ModalMessageDaten}
                                        </Text>
                                    </View>
                                </ScrollView>
                            </View>
                            <View style={styles.modalButtons1}>
                                <View style={styles.modalButtonsinside1}>
                                    <Button onPress={() => this. handleDatenSchutz(0)} colorbutton="yellow_small">
                                        <Text style={styles.modalText}>{stringSelector('agree')}</Text>
                                    </Button>
                                    <Button onPress={() => this. handleDatenSchutz(1)} colorbutton="yellow_small">
                                        <Text style={styles.modalText}>{stringSelector('no')}</Text>
                                    </Button>
                                </View>
                            </View>
                            <View style={styles.modalSeparator1}></View>
                        </View>
                        <View style={styles.modalSeparator1}></View>
                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.showWarningModal}
                >
                    <View style={styles.questionModalBackground}>
                        <View style={styles.modalSeparator}></View>

                        <View style={styles.questionModal}>
                            <TouchableOpacity style={styles.closeQuestionModal} onPress={() => this.setState({ showWarningModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.wariningModalText}>
                                {this.state.ModalMessage}
                            </Text>
                            <Button onPress={() => this.setState({ showWarningModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>

                    </View>
                </Modal>
                <ImageBackground
                    source={require('../../assets/backgrounds/dopaminePageBackground.png')}
                    style={styles.dopamineScreenBackground}
                >
                    <View style={styles.containerTop}>
                        <View style={styles.urbeesDisplayer}>
                            <View style={styles.locationwrapper}>
                                <Text style={styles.nameOfPlace}>
                                    {this.state.cityname}
                                </Text>
                            </View>
                            <View style={styles.urbeeswrapper}>
                                <Image
                                    source={require('../../assets/urbeecriptocurrency.png')}
                                    resizeMode='contain'
                                    style={styles.urbeeCoin}
                                ></Image>
                                <Text style={styles.urbeesNumber}>
                                    {this.state.urbees}
                                </Text>
                            </View>
                        </View>
                        <View style={styles.separator}></View>
                    </View>
                    <View style={styles.containerBottom}>
                        <TouchableOpacity onPress={() => this.getUrbeesForWalking()}>
                            <Image
                                source={this.state.ImageBeeHive}
                                resizeMode='contain'
                                style={styles.beehive}
                            ></Image>
                            <Text style={styles.collectText}>
                                {stringSelector('taptocolecttext')}
                            </Text>
                        </TouchableOpacity>
                    </View>
                    {//loading icon only shown when "ready" variable is false
                        !this.state.ready && (
                            <View style={styles.loadingIcon}>
                                <ActivityIndicator size='large' color='#FFFC71' />
                            </View>
                        )}
                </ImageBackground>
                 {//loading icon only shown when "ready" variable is false
                    !this.state.ready && (
                        <View style={styles.loadingIcon}>
                            <ActivityIndicator size='large' color='#FFFC71' />
                        </View>
                    )}
            </View >
        );
    }
}



const styles = StyleSheet.create({
    container: {
        justifyContent: "center", //This center vertically when flex-direction is Column
        flex: 1,
    },
    dopamineScreenBackground: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerTop: {
        flex: 40,
        width: '100%',
        alignItems: "center",
        justifyContent: "center",
    },
    urbeesDisplayer: {
        flex: 3,
        width:"95%",
        flexDirection: "row",
        justifyContent:"space-between"
    },
    separator: {
        flex: 8,
    },
    honeyHexagon: {
        width: 50,
        height: 50,
        alignSelf: "center",
        flex: 3,
    },
    nameOfPlace: {
        fontSize: 30,
        fontWeight: 'bold',
        alignSelf: 'center',
        color: 'white',
        textShadowRadius: 30,
        textShadowColor: 'grey',
    },
    urbeeCoin: {
        width: 35,
        height: 35,
        alignSelf: "center",
    },
    urbeesNumber: {
        fontSize: 30,
        fontWeight: 'bold',
        alignSelf: 'center',
        color: 'white',
        textShadowRadius: 30,
        textShadowColor: 'grey',
    },
    containerBottom: {
        flex: 30,
        width: '100%',
        alignContent: 'center',
    },

    beehive: {
        width: '100%',
        height: "80%",
        alignSelf: "center",
    },
    collectText: {
        fontSize: 30,
        color: "lightgrey",
        fontWeight: 'bold',
        textAlign: 'center',
        textAlignVertical: 'top',
    },
    questionModalBackground: {
        backgroundColor: '#000000aa',
        flex: 1
    },
    questionModal: {
        width: "80%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    closeQuestionModal: {
        position: 'absolute',
        top: 30,
        right: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.1)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalTitle: {
        fontSize: 20,
    },
    modalquestion: {
        fontSize: 15,
        marginVertical: 10,
    },
    questionModalButtons: {
        alignItems: 'center',
    },
    modalText: {
        fontSize: 15,
    },
    modalButton: {
        backgroundColor: 'yellow',
        borderRadius: 10,
        marginHorizontal: 10,
        marginBottom: 10,
    },
    wariningModalText: {
        fontSize: 20,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    loadingIcon: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalSeparator: {
        flex: 2,
    },
    urbeeswrapper: {
        flexDirection: "row",
        
    },
    locationwrapper: {
       
        flexDirection: "row",
    },
    loadingIcon:{
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    questionModalBackground1: {
        backgroundColor: '#000000aa',
        flex: 1,
    },
    questionModal1: {
        width: "80%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        alignContent: "center",
        alignItems: "center",
        alignSelf: "center",
        display:"flex",
    },
    closeQuestionModal1: {
        position: 'absolute',
        top: 30,
        right: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.1)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalTitle1: {
        fontSize: 20,
    },
    modalquestion1: {
        fontSize: 15,
        marginVertical: 10,
    },
    questionModalButtons1: {
        alignItems: 'center',
    },
    modalButtons1: {
        flex:2,
    },
    modalButtonsinside1:{
        flexDirection:"row",
        justifyContent: 'center',
    },
    wariningModalTextContainer1:{
        flex:8,
    },
    modalText1: {
        fontSize: 15,
    },
    modalButton1: {
        backgroundColor: 'yellow',
        borderRadius: 10,
        marginHorizontal: 10,
        marginBottom: 10,
    },
    wariningModalText1: {
        fontSize: 10,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    modalSeparator1: {
        flex: 2,
    },

});