import React, { useState } from 'react';
import {  StyleSheet, Text, View, TouchableOpacity, LayoutAnimation,FlatList, Image, ImageBackground, EventEmitter  } from 'react-native';
import {createAppContainer} from 'react-navigation';
import AppNavigator from '../items/storeNavigator';
const StoreMenu = createAppContainer(AppNavigator);
export default class ProductsScreen extends React.Component {
    static navigationOptions = {
        headerShown: false
    };  
    render(){ 
        //This is for the uper buttons of the challenges to show each one 
        LayoutAnimation.easeInEaseOut();
        return( 
        <View style={styles.container}>
          <StoreMenu />
          </View>
        );
    }  

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF"
      }
});