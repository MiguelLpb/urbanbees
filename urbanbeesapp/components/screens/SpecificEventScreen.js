import React, { useState } from 'react';
import { StyleSheet, Text, View,ScrollView, ImageBackground, ActivityIndicator, Linking, TouchableOpacity, Modal } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { Button } from '../Button';
import { getDistance } from 'geolib';
import { Input } from '../items/input';
import * as firebase from 'firebase';
import fire from '../../config';
import stringSelector from '../lenguage/lenguages';
export default class SpecificEventScreen extends React.Component {
    static navigationOptions = {
        headerShown: false
    };
    state = {
        eventId: '',
        name: '',
        address: '',
        description: '',
        lat: '',
        lon: '',
        image: 'https://firebasestorage.googleapis.com/v0/b/urbanbees-e9017.appspot.com/o/Atmospheric%20CO2%20in%20years%20-%20Graph.jpg?alt=media&token=c9b0f816-3c2b-4de3-9cc0-4f0a97c342d8',
        link: '',
        urbees: '',
        city: global.cityfortheapp,
        beginen: '',
        end: '',
        code: '',
        date: '',
        expired: '',
        numberOfUsers: '',
        reservationStartDate: '',
        codeGiven: '',
        showCongratsModal: false,
        showWarningModal: false,
        distanceFromPlace: 0,
        activeClaimButton: true,
        userlat: 0,
        userlon: 0,
        ready: true,
        distance: 0,
        message: '',
        userReserved: false,
        reservationFull: false,
        reservationTime: false,
    };
    //Get all the info from this Event from firebase
    getEvent = () => {
        fire.database().ref('/Cities/' + this.state.city + '/Events/' + this.state.eventId).once('value', snapshot => {
            this.setState(
                {
                    name: snapshot.child('name').val(),
                    address: snapshot.child('address ').val(),
                    description: snapshot.child('description').val(),
                    lat: snapshot.child('lat').val(),
                    lon: snapshot.child('lon').val(),
                    image: snapshot.child('image').val(),
                    link: snapshot.child('link').val(),
                    urbees: snapshot.child('urbees').val(),
                    beginen: snapshot.child('beginen').val(),
                    end: snapshot.child('end').val(),
                    code: snapshot.child('code').val(),
                    date: snapshot.child('date').val(),
                    expired: snapshot.child('expired').val(),
                    numberOfUsers: snapshot.child('numberofusers').val(),
                    reservationStartDate: snapshot.child('reservation_from').val(),
                }
            )

        }).then(() => this.reservationTimeCheck());
    }
    loadLinkInBrowser = () => {
        Linking.openURL(this.state.link).catch(err => console.error("Couldn't load page", err));
    };
    openGps = () => {
        //depending on the platform the url is diferent 
        var scheme = Platform.OS === 'ios' ? 'https://maps.apple.com/?q' : 'https://maps.google.com/?q';
        var url = scheme + this.state.lat + ',' + this.state.lon;
        Linking.openURL(scheme + '=' + this.state.address + '&ll=' + url);
    }
    //Check if the user already claimed Urbees for this event, and if yes dactivate button 
    checkIfuserAlreadyWent = () => {
        var IdofUser = firebase.auth().currentUser.uid;
        firebase.database().ref('/Cities/' + this.state.city + '/Events/' + this.state.eventId + '/Users/').once('value', snapshot => {
            if (snapshot.child(IdofUser).exists()) {
                if (!(snapshot.child(IdofUser).child('went').val() == 'yes')) {
                    this.setState({ userReserved: true });
                } else {
                    this.setState({ activeClaimButton: false });
                }
            } else {

            }
            if (this.state.numberOfUsers == 0) {
                this.setState({ reservationFull: true });
            }
        });
    }
    reservationTimeCheck = () => {
        let reserveDateString = this.state.reservationStartDate;
        let reserveDateParts = reserveDateString.split(".");
        let reserveDate = new Date(+reserveDateParts[2], reserveDateParts[1] - 1, +reserveDateParts[0]); //This converts our date format from the database to the format for Javascript
        let today = new Date();
        if (today.getTime() > reserveDate.getTime()) {
            this.setState({ reservationTime: true });
        }
        this.checkIfuserAlreadyWent();
    }
    reserveOrUnreserveTheUser = (action) => {
        var IdofUser = firebase.auth().currentUser.uid;
        firebase.database().ref('/Cities/' + this.state.city + '/Events/' + this.state.eventId + '/Users/').once('value', snapshot => {
            if (action == 0) {
                snapshot.child(IdofUser).child('went').ref.set('no');
                firebase.database().ref('/Cities/' + this.state.city + '/Events/' + this.state.eventId + '/numberofusers').once('value', snapshot2 => {
                    let newNumberofusersreserved = snapshot2.val() - 1;
                    snapshot2.ref.set(newNumberofusersreserved);
                    if (newNumberofusersreserved == 0) {
                        this.setState({ reservationFull: true });
                    }
                });
                this.setState({ userReserved: true });
            } else {
                snapshot.child(IdofUser).ref.remove();
                this.setState({ userReserved: false });
                firebase.database().ref('/Cities/' + this.state.city + '/Events/' + this.state.eventId + '/numberofusers').once('value', snapshot2 => {
                    let newNumberofusersreserved = snapshot2.val() + 1;
                    snapshot2.ref.set(newNumberofusersreserved);
                    this.setState({ reservationFull: false });
                });

            }

        });
    }
    //Function that with the lat and lon of the place checks if the user is in the same place as this one
    validateUserLocation = () => {
        if (this.state.code == this.state.codeGiven) {
            this.setState({ ready: false });
            let geoOptions = {
                enableHighAccuracy: true,
                timeOut: 20000,
                maximumAge: 60 * 60 * 24
            };
            navigator.geolocation.getCurrentPosition(this.geoSucces, this.geoFailure, geoOptions);
        } else {
            this.setState({ message: stringSelector('wrong_code_title') + ': ' + stringSelector('wrong_code_text') });
            this.setState({ showWarningModal: true });
        }
    }
    geoSucces = (position) => {
        this.setState({ userlat: position.coords.latitude });
        this.setState({ userlon: position.coords.longitude });
        var totaldistance = getDistance(
            { latitude: this.state.lat, longitude: this.state.lon },
            { latitude: this.state.userlat, longitude: this.state.userlon }
        );
        this.setState({ distance: totaldistance });
        this.checkUserLocationAndGiveUrbees(this.state.distance);
    }
    geoFailure = (err) => {
        this.setState({ message: stringSelector('apologies_default_text') + err });
        this.setState({ showWarningModal: true });
        this.setState({ ready: true });
    }
    componentDidMount() {
        this.setState(
            {
                eventId: this.props.navigation.getParam('eventId', 'no')
            }, () => { // this is for the getEvent function to execute after the setstate is done, otherwise it executes before the event id is set and the database returns null
                this.getEvent();
            });
    }
    //This Function gives the urbees to the user, sets the level of the ExporerBee and switches the user to already went so he can not collect again
    checkUserLocationAndGiveUrbees = (validatedlocation) => {
        var IdofUser = firebase.auth().currentUser.uid;
        if (validatedlocation < 150) {
            firebase.database().ref('/Cities/' + this.state.city + '/Events/' + this.state.eventId + '/Users/' + IdofUser + '/went').set('yes').then(() => {
                fire.database().ref('/Users/' + IdofUser).once('value', snapshot => {
                    let newUrbees = snapshot.child('urbees').val() + this.state.urbees;
                    snapshot.child('urbees').ref.set(newUrbees);
                    var newEventtimes = 0;
                    //level up the exporerBee
                    if (snapshot.child('eventtimes').exists) {
                        newEventtimes = snapshot.child('eventtimes').val() + 1;
                    } else {
                        newEventtimes = 1;
                    }
                    snapshot.child('eventtimes').ref.set(newEventtimes);
                }).then(() => {
                    this.setState({ message: 'Congrats, you earned ' + this.state.urbees + ' Urbees' });
                    this.setState({ showCongratsModal: true });
                    this.setState({ activeClaimButton: false });
                    this.setState({ ready: true });
                    this.setState({ codeGiven: '' });
                });
            });
        } else {
            this.setState({ message: stringSelector('not_at_distance_title1') + validatedlocation + stringSelector('not_at_distance_title2') + '.  ' + stringSelector('not_at_distance_text') });
            this.setState({ showWarningModal: true });
            this.setState({ ready: true });
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <Modal
                    transparent={true}
                    visible={this.state.showWarningModal}
                >
                    <View style={styles.modalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.modal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ showWarningModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.wariningModalText}>
                                {this.state.message}
                            </Text>
                            <Button onPress={() => this.setState({ showWarningModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.showCongratsModal}
                >
                    <View style={styles.modalBackground}>
                        <View style={styles.modalSeparator}></View>
                        <View style={styles.modal}>
                            <TouchableOpacity style={styles.closeModal} onPress={() => this.setState({ showCongratsModal: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <Text style={styles.modalText}>
                                {this.state.message}
                            </Text>
                            <Button onPress={() => this.setState({ showCongratsModal: false })} colorbutton="yellow">
                                <Text style={styles.modalText}>{stringSelector('ok')}</Text>
                            </Button>
                        </View>
                        <View style={styles.modalSeparator}></View>
                    </View>
                </Modal>
                <TouchableOpacity style={styles.back} onPress={() => this.props.navigation.goBack()}>
                    <Ionicons name='ios-arrow-back' size={32} color='black'></Ionicons>
                </TouchableOpacity>
                <View style={styles.scrollView}>
                    <ScrollView style={styles.scrollViewScroll}>
                        <View style={styles.containerOfContainers}>
                            <View style={styles.containerTop}>
                                <ImageBackground
                                    source={{ uri: this.state.image }}
                                    style={styles.imageOfPlace}
                                >
                                    <Text style={styles.nameOfPlace}>
                                        {this.state.name}
                                    </Text>
                                </ImageBackground>
                            </View>
                            <View style={styles.containerBottom}>
                                <Text style={styles.descriptionOfPlace}>
                                    {this.state.description + ' ' + this.state.date}
                                </Text>
                                <Text style={styles.descriptionOfPlace}>
                                    {stringSelector("address_info")}
                                </Text>
                                <Text style={styles.descriptionOfPlace}>
                                    {this.state.address}
                                </Text>
                                <View style={styles.twoButtonsAtBottom}>
                                    <Button colorbutton="orange_small" onPress={this.openGps}>Location</Button>
                                    <View style={styles.buttonSeparator}></View>
                                    <Button colorbutton="orange_small" onPress={this.loadLinkInBrowser} >More info</Button>
                                </View>
                                {//If the reservation for the event is not full you can reserve 
                                    (this.state.activeClaimButton && this.state.reservationTime && !this.state.reservationFull) && (
                                        <View style={styles.goButton}>
                                            <Button
                                                colorbutton="light_orange"
                                                onPress={(!this.state.userReserved) ? () => this.reserveOrUnreserveTheUser(0) : () => this.reserveOrUnreserveTheUser(1)}
                                            >
                                                {(!this.state.userReserved) ? stringSelector('Reservation_text') : stringSelector('Reserved_event_calcel_button')}
                                            </Button>
                                        </View>
                                    )}
                                {//If the reservation time is not yet, show a message with the date for reserving
                                    !this.state.reservationTime && (
                                        <Text style={styles.extrainfo}>
                                            {stringSelector('Reserved_event_time') + this.state.reservationStartDate}
                                        </Text>
                                    )}
                                {//If the reservations are full show message 
                                    this.state.reservationFull && (
                                        <View style={styles.goButton}>
                                            <Text style={styles.extrainfo}>
                                                {stringSelector('full_reservation')}
                                            </Text>
                                            <Button
                                                colorbutton="light_orange"
                                                onPress={(!this.state.userReserved) ? () => {/* */ } : () => this.reserveOrUnreserveTheUser(1)}
                                            >
                                                {(!this.state.userReserved) ? stringSelector('Reserved_not_possible') : stringSelector('Reserved_event_calcel_button')}
                                            </Button>
                                        </View>
                                    )}

                               
                            </View>
                        </View>
                    </ScrollView>
                </View>
                <View style={styles.codeAndclaimButtonContainer}>
                    <View style={styles.codeInput}>
                        <Input
                            placeholder='Code'
                            onChangeText={codeGiven => this.setState({ codeGiven })}
                            secureTextEntry
                            value={this.state.codeGiven}
                            color={"white"}
                        />
                    </View>
                    {//If the reservation time is not yet, dont show even the button to claim 
                    (this.state.reservationTime && !this.state.reservationFull) && (
                        <View style={styles.goButton}>
                            <Button
                                colorbutton="white"
                                onPress={this.state.activeClaimButton ? () => this.validateUserLocation() : () => {/*Do nothing if the user was there already*/ }}
                            >
                                {this.state.activeClaimButton ? stringSelector('claimUrbees') + " " + this.state.urbees + " " + stringSelector('get_urbees_part2') : stringSelector('already_claimed_button_event')}
                            </Button>
                        </View>
                    )}
                     {//If the reservations are full show message 
                    this.state.reservationFull && (
                        <View style={styles.goButton}>
                            <Button
                                colorbutton="white"
                                onPress={(this.state.activeClaimButton && this.state.userReserved) ? () => this.validateUserLocation() : () => {/*Do nothing if the user was there already*/ }}
                            >
                                {(this.state.activeClaimButton && this.state.userReserved) ? 'Collect ' + this.state.urbees + ' Urbees' : 'The event is full'}
                            </Button>
                        </View>
                    )}
                </View>
                {//loading icon only shown when "ready" variable is false
                    !this.state.ready && (
                        <View style={styles.loadingIcon}>
                            <ActivityIndicator size='large' color='#FFFC71' />
                        </View>
                    )}
            </View >
        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFFFFF",
    },
    containerTop: {
        flex: 35,

    },

    imageOfPlace: {
        resizeMode: "cover",
        justifyContent: "flex-end",
        flex: 1,
        height: 300,

    },
    containerBottom: {
        flex: 65,
        alignItems: "center",
        marginTop:5,
    },
    nameOfPlace: {
        fontSize: 18,
        fontWeight: 'bold',
        color: "white",
        marginLeft: 10,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10
    },
    addressOfPlace: {
        color: "white",
        fontWeight: 'bold',
        fontSize: 18,
        marginLeft: 10,
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10,
    },
    descriptionOfPlace: {
        width: '90%',
        color: "black",
        fontSize: 17,
        marginBottom: 3,
        textAlign: "justify",
    },
    extrainfo: {
        width: '90%',
        color: "black",
        fontSize: 17,
        marginBottom: 3,
        textAlign: "center",
        marginTop:5,
    },
    goButton: {
      width:"100%",
      alignItems: "center",
    },
    codeInput: {
        alignItems: "center",
    }, 
    twoButtonsAtBottom: {
        flexDirection: "row",
        justifyContent: 'center',
    },
    moreInfoButton: {
        width: '30%',
        height: 100,
        alignSelf: 'center',
    },

    locationButton: {
        width: '30%',
        height: 100,
        alignSelf: 'center',

    },
    modalBackground: {
        backgroundColor: '#000000aa',
        flex: 1
    },
    modal: {
        width: "60%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        justifyContent: "center",
        alignContent: "center",
        alignItems: "center",
        alignSelf: "center",
    },
    closeModal: {
        position: 'absolute',
        top: 15,
        right: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0)',
    },
    wariningModalText: {
        fontSize: 17,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    modalText: {
        fontSize: 17,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    modalButton: {
        backgroundColor: 'yellow',
        borderRadius: 10,
        marginHorizontal: 10,
        marginBottom: 10,
    },
    loadingIcon: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalSeparator: {
        flex: 4,
    },
    modalSeparatorsmall: {
        flex: 1,
    },
    back: {
        zIndex:1,
        position: 'absolute',
        top: 10,
        left: 15,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    codeAndclaimButtonContainer:{
        alignItems: "center",
        backgroundColor:'#fffc71',
        paddingBottom:10,
    },
    scrollView:{
        flex: 85,
    },
    buttonSeparator:{
        marginHorizontal:3,
    }
});