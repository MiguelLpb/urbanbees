import React, { useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Modal, ScrollView } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import * as Google from 'expo-google-app-auth';
import fire from '../../config';
import * as Facebook from 'expo-facebook';
import * as firebase from 'firebase';
import { Button } from '../Button';
const facebookAppId = "345922226020113";
const facebookAppName = "UrbanBees";
import stringSelector from '../lenguage/lenguages';
import * as AppleAuthentication from 'expo-apple-authentication';
import * as Crypto from "expo-crypto";
export default class AuthenticationScreen extends React.Component {

    state = {
        error: '',
        showWarningModalfacebook: false,
        showWarningModalgoogle: false,
        ModalMessage: stringSelector("datenschutzenglish"),
        loginAvailable: false,
    };
    componentDidMount() {

        this.checkIfLoginAppleIsAvailable();

        firebase.auth().onAuthStateChanged((user) => {
            if (user != null) {
                console.log(user);
            }
        }
        )
    }
    checkIfLoginAppleIsAvailable = async () => {
        const appleloginavailable = await AppleAuthentication.isAvailableAsync()
        this.setState({ loginAvailable: appleloginavailable });

    }
    //facebook login function:
    loginWithFacebook = async () => {
        Facebook.initializeAsync(facebookAppId, facebookAppName);
        const { type, token } = await Facebook.logInWithReadPermissionsAsync(facebookAppId, { permissions: ['public_profile', 'email'], behavior: 'web' });
        if (type === 'success') {
            const credential = firebase.auth.FacebookAuthProvider.credential(token);
            try {
                firebase.auth().signInWithCredential(credential).then(function (res) {
                    if (res.additionalUserInfo.isNewUser) {
                        fire.database().ref('/Users/' + res.user.uid).set({
                            eventtimes: 0,
                            fristtime: 'yes',
                            firsttimechallenges: 'yes',
                            firsttimenews: 'yes',
                            firsttimeplaces: 'yes',
                            firsttimestore: 'yes',
                            learningtimes: 0,
                            mobilitytimes: 0,
                            name: res.user.displayName.trim(),
                            turisticttimes: 0,
                            urbees: 0,
                            useremail: res.user.email,
                        })
                        console.log('user signed in');
                    }
                }).catch(function (errorfacebook) {
                    // Handle Errors here.
                    var errorCode = errorfacebook.code;
                    var errorMessage = errorfacebook.message;
                    // The email of the user's account used.
                    var email = errorfacebook.email;
                    // The firebase.auth.AuthCredential type that was used.
                    var credential = errorfacebook.credential;
                    // ...
                    console.log('si entra el error' + "errorCode: " + errorCode + " errorMessage: " + errorMessage + " email: " + email + " credential: " + credential);
                    //this.setState({error:"errorCode: "+errorCode+" errorMessage: "+errorMessage+" email: "+email+" credential: "+credential} );
                });
            } catch (e) {
                console.log(e);
            }
        }

    }

    //User check if already signIn with google from the javascript firebase web google login documentation
    isUserEqual = (googleUser, firebaseUser) => {
        if (firebaseUser) {
            var providerData = firebaseUser.providerData;
            for (var i = 0; i < providerData.length; i++) {
                if (providerData[i].providerId === firebase.auth.GoogleAuthProvider.PROVIDER_ID &&
                    providerData[i].uid === googleUser.getBasicProfile().getId()) {
                    // We don't need to reauth the Firebase connection.
                    return true;
                }
            }
        }
        return false;
    }
    //google signIn from the javascript firebase web google login documentation
    onSignIn = (googleUser) => {
        console.log('Google Auth Response', googleUser);
        // We need to register an Observer on Firebase Auth to make sure auth is initialized.
        var unsubscribe = firebase.auth().onAuthStateChanged(function (firebaseUser) {
            unsubscribe();
            // Check if we are already signed-in Firebase with the correct user.
            if (!this.isUserEqual(googleUser, firebaseUser)) {
                // Build Firebase credential with the Google ID token.
                var credential = firebase.auth.GoogleAuthProvider.credential(
                    googleUser.idToken,
                    googleUser.accessToken
                );
                // Sign in with credential from the Google user.   
                firebase.auth().signInWithCredential(credential).then(function (res) {
                    if (res.additionalUserInfo.isNewUser) {
                        fire.database().ref('/Users/' + res.user.uid).set({
                            eventtimes: 0,
                            fristtime: 'yes',
                            firsttimechallenges: 'yes',
                            firsttimenews: 'yes',
                            firsttimeplaces: 'yes',
                            firsttimestore: 'yes',
                            learningtimes: 0,
                            mobilitytimes: 0,
                            name: res.additionalUserInfo.profile.given_name,
                            turisticttimes: 0,
                            urbees: 0,
                            useremail: res.user.email,
                        })
                        console.log('user signed in');
                    }
                }).catch(function (errorGoogle) {
                    // Handle Errors here.
                    var errorCode = errorGoogle.code;
                    var errorMessage = errorGoogle.message;
                    // The email of the user's account used.
                    var email = errorGoogle.email;
                    // The firebase.auth.AuthCredential type that was used.
                    var credential = errorGoogle.credential;
                    // ...
                    this.setState({ error: "errorCode: " + errorCode + " errorMessage: " + errorMessage + " email: " + email + " credential: " + credential });
                });
            } else {
                console.log('User already signed-in Firebase.');
            }
        }.bind(this));
    }
    //Goole sign in from the google console 
    signInWithGoogleAsync = async () => {
        try {
            const result = await Google.logInAsync({
                behavior: 'web',
                androidClientId: '678903835124-d9vncneaigq76ooettfqgce3h6p70h1v.apps.googleusercontent.com',
                iosClientId: '678903835124-bulm6iaa802vsilcs44f7osabb2u3kvl.apps.googleusercontent.com',
                androidStandaloneAppClientId: '678903835124-j5lr1c0cop2rak2ma37j4hf2uhs1ukj2.apps.googleusercontent.com',
                iosStandaloneAppClientId: "678903835124-lht2c71nunriavaf1jrpqgrvvscib3mv.apps.googleusercontent.com",
                scopes: ['profile', 'email'],
            });

            if (result.type === 'success') {
                this.onSignIn(result);
                return result.accessToken;
            } else {
                return { cancelled: true };
            }
        } catch (e) {
            return { error: true };
        }
    }
    //Apple registration
    RegisterWithApple = async () => {
        try {
            const csrf = Math.random().toString(36).substring(2, 15);
            const nonce = Math.random().toString(36).substring(2, 10);
            const hashedNonce = await Crypto.digestStringAsync(Crypto.CryptoDigestAlgorithm.SHA256, nonce);
            const credential = await AppleAuthentication.signInAsync({
                requestedScopes: [
                    AppleAuthentication.AppleAuthenticationScope.FULL_NAME,
                    AppleAuthentication.AppleAuthenticationScope.EMAIL,
                ],
                state: csrf,
                nonce: hashedNonce
            });
            // signed in
            const { identityToken, email, state, fullName } = credential;


            if (identityToken) {
                const provider = new firebase.auth.OAuthProvider("apple.com");
                provider.addScope('email');
                provider.addScope('name');
                const credential = provider.credential({
                    idToken: identityToken,
                    rawNonce: nonce // nonce value from above
                });
                // Sign in with credential from the Google user.   
                firebase.auth().signInWithCredential(credential).then(function (res) {
                    console.log('Test apple fullName: ' + JSON.stringify(firebase.auth().currentUser.displayName));
                    if (res.additionalUserInfo.isNewUser) {
                        var usernameapple = (JSON.stringify(firebase.auth().currentUser.displayName) == 'null' )?'Profile':firebase.auth().currentUser.displayName;
                        fire.database().ref('/Users/' + res.user.uid).set({
                            eventtimes: 0,
                            fristtime: 'yes',
                            firsttimechallenges: 'yes',
                            firsttimenews: 'yes',
                            firsttimeplaces: 'yes',
                            firsttimestore: 'yes',
                            learningtimes: 0,
                            mobilitytimes: 0,
                            name: usernameapple,
                            turisticttimes: 0,
                            urbees: 0,
                            useremail: res.user.email,
                        })
                        console.log('user signed in');
                    }
                }).catch(function (error) {
                    //this.setState({error:"errorCode: "+error} );
                    console.log("error firebase: " + error);
                });
            }

        } catch (e) {
            if (e.code === 'ERR_CANCELED') {
                // handle that the user canceled the sign-in flow
                console.log(e.code);
                return { error: true };
            } else {
                // handle other errors
                console.log(e);
                return { error: true };
            }
        }
    }

    static navigationOptions = {
        headerShown: false
    };
    goToLogin = () => {
        this.props.navigation.navigate("Login"); //navigate to Login page after click 
    };
    goToRegister = () => {
        this.props.navigation.navigate("Register"); //navigate to Register page after click 
    };
    render() {
        return (
            <View style={styles.container}>
                <Modal
                    transparent={true}
                    visible={this.state.showWarningModalfacebook}
                >
                    <View style={styles.questionModalBackground}>
                        <View style={styles.modalSeparator}></View>

                        <View style={styles.questionModal}>
                            <TouchableOpacity style={styles.closeQuestionModal} onPress={() => this.setState({ showWarningModalfacebook: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <View style={styles.modalSeparator}></View>
                            <View style={styles.wariningModalTextContainer}>
                                <ScrollView>
                                    <View style={styles.wariningModalText}>
                                        <Text >
                                            {this.state.ModalMessage}
                                        </Text>
                                    </View>
                                </ScrollView>
                            </View>
                            <View style={styles.modalButtons}>
                                <View style={styles.modalButtonsinside}>
                                    <Button onPress={() => this.loginWithFacebook()} colorbutton="yellow_small">
                                        <Text style={styles.modalText}>{stringSelector('agree')}</Text>
                                    </Button>
                                    <Button onPress={() => this.setState({ showWarningModalfacebook: false })} colorbutton="yellow_small">
                                        <Text style={styles.modalText}>{stringSelector('no')}</Text>
                                    </Button>
                                </View>
                            </View>
                            <View style={styles.modalSeparator}></View>
                        </View>
                        <View style={styles.modalSeparator}></View>

                    </View>
                </Modal>
                <Modal
                    transparent={true}
                    visible={this.state.showWarningModalgoogle}
                >
                    <View style={styles.questionModalBackground}>
                        <View style={styles.modalSeparator}></View>

                        <View style={styles.questionModal}>
                            <TouchableOpacity style={styles.closeQuestionModal} onPress={() => this.setState({ showWarningModalgoogle: false })}>
                                <AntDesign name="closecircleo" size={24} color="black" />
                            </TouchableOpacity>
                            <View style={styles.modalSeparator}></View>
                            <View style={styles.wariningModalTextContainer}>
                                <ScrollView>
                                    <View style={styles.wariningModalText}>
                                        <Text >
                                            {this.state.ModalMessage}
                                        </Text>
                                    </View>
                                </ScrollView>
                            </View>
                            <View style={styles.modalButtons}>
                                <View style={styles.modalButtonsinside}>
                                    <Button onPress={() => this.signInWithGoogleAsync()} colorbutton="yellow_small">
                                        <Text style={styles.modalText}>{stringSelector('agree')}</Text>
                                    </Button>
                                    <Button onPress={() => this.setState({ showWarningModalgoogle: false })} colorbutton="yellow_small">
                                        <Text style={styles.modalText}>{stringSelector('no')}</Text>
                                    </Button>
                                </View>
                            </View>
                            <View style={styles.modalSeparator}></View>
                        </View>
                        <View style={styles.modalSeparator}></View>

                    </View>
                </Modal>
                <View style={styles.containerTop}>
                    <Image
                        source={require('../../assets/splash.png')}
                        resizeMode='contain'
                        style={styles.logo}
                    ></Image>
                    <Text style={styles.appName}> UrbanBees </Text>
                    <Text style={styles.errormessageforlogin}> {this.state.error} </Text>
                </View>
                <View style={styles.containerBottom}>
                    <Button
                        colorbutton="white"
                        onPress={() => this.goToLogin()}
                    >
                        Login
                    </Button>
                    <Button
                        colorbutton="white"
                        onPress={() => this.signInWithGoogleAsync()}
                    >
                        Google
                        </Button>
                    <Button
                        colorbutton="white"
                        onPress={() => this.loginWithFacebook()}
                    >
                        Facebook
                        </Button>
                    {this.state.loginAvailable === true ? (
                        <AppleAuthentication.AppleAuthenticationButton
                            buttonType={AppleAuthentication.AppleAuthenticationButtonType.SIGN_IN}
                            buttonStyle={AppleAuthentication.AppleAuthenticationButtonStyle.BLACK}
                            cornerRadius={20}
                            style={{
                                marginTop: 10,
                                width: '80%',
                                height: 40,
                                borderRadius: 20,
                                paddingVertical: 10,
                                alignItems: 'center',
                            }}
                            onPress={() => this.RegisterWithApple()}
                        />
                    ) : null}

                    <Text style={styles.registerButtonQuestionText}> Do not have an account?  </Text>
                    <TouchableOpacity
                        style={styles.registerButton}
                        onPress={() => this.goToRegister()}
                    >
                        <Text style={styles.registerButtonText}>  Register here </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }

}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fffc71",
    },
    containerTop: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    containerBottom: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo: {
        width: "100%",
        height: 100,
    },
    appName: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    errormessageforlogin: {
        fontSize: 14,
        fontWeight: 'bold',
        color: "red",
        textAlign: "center",
    },
    loginButton: {
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: 'black',
        width: 220,
        marginBottom: 20
    },
    loginButtonText: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold'
    },
    registerButtonQuestionText: {
        fontSize: 15,
        fontWeight: 'bold',
        marginTop: 10,
    },
    registerButtonText: {
        fontSize: 15,
        fontWeight: 'bold',
        color: '#71bbff'
    },
    socialMediaButtons: {

    },
    googleButton: {
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: 'black',
        width: 100,
        marginEnd: 20
    },
    googleButtonText: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold'

    },
    facebookButton: {
        borderStyle: 'solid',
        borderWidth: 2,
        borderColor: 'black',
        width: 100,
    },
    facebookButtonText: {
        textAlign: 'center',
        fontSize: 20,
        fontWeight: 'bold'
    },
    questionModalBackground: {
        backgroundColor: '#000000aa',
        flex: 1,
    },
    questionModal: {
        width: "80%",
        borderRadius: 10,
        backgroundColor: '#ffffff',
        flex: 6,
        alignContent: "center",
        alignItems: "center",
        alignSelf: "center",
        display: "flex",
    },
    closeQuestionModal: {
        position: 'absolute',
        top: 30,
        right: 22,
        width: 32,
        height: 32,
        borderRadius: 16,
        backgroundColor: 'rgba(21,22,48,0.1)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalTitle: {
        fontSize: 20,
    },
    modalquestion: {
        fontSize: 15,
        marginVertical: 10,
    },
    questionModalButtons: {
        alignItems: 'center',
    },
    modalButtons: {
        flex: 2,
    },
    modalButtonsinside: {
        flexDirection: "row",
        justifyContent: 'center',
    },
    wariningModalTextContainer: {
        flex: 8,
    },
    modalText: {
        fontSize: 15,
    },
    modalButton: {
        backgroundColor: 'yellow',
        borderRadius: 10,
        marginHorizontal: 10,
        marginBottom: 10,
    },
    wariningModalText: {
        fontSize: 10,
        color: 'black',
        alignSelf: "center",
        width: "80%",
        textAlign: 'justify',
    },
    modalSeparator: {
        flex: 2,
    },
});