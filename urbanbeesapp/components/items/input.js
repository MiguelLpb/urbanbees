import React from 'react';
import {View, StyleSheet, Text, TextInput} from 'react-native';

const Input = ({label, value, onChangeText, placeholder,multiline,numberOfLines, secureTextEntry,color})=>{
    
    return (
        <View style={StyleSheet.container}>
        <Text style={styles.label}>{label}</Text>
        <TextInput
            autoCorrect= {false}
            onChangeText={onChangeText}
            placeholder={placeholder}
            placeholderTextColor={color}
            style={styles.input}
            secureTextEntry={secureTextEntry}
            multiline={multiline}
            numberOfLines={numberOfLines}
            value={value}
        />
        </View>
    )
}
const styles = StyleSheet.create ({
    container: {
        marginTop: 10,
        width: '100%',
        borderColor: 'black',
        borderBottomWidth: 2,
    },
    label:{
        padding: 5, 
        paddingBottom: 0,
        color: 'black',
        fontSize: 17,
        fontWeight: '700',
        width: '100%',
    },
    input: {
        borderColor:'black',
        borderBottomWidth: 2,
        paddingHorizontal: 5, 
        color: 'black',
        fontSize: 18,
        fontWeight: '700',
        width: 220,
        textAlign:"center"
    }
});
export {Input};