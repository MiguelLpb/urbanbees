import React from 'react';
import { Image } from 'react-native';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import LessonsScreen from '../screens/LessonsScreen';
import SpecificLessonScreen from '../screens/SpecificLessonScreen';
import PlacesScreen from '../screens/PlacesScreen';
import SpecificPlaceScreen from '../screens/SpecificPlaceScreen';
import EventsScreen from '../screens/EventsScreen';
import SpecificEventScreenn from '../screens/SpecificEventScreen';
//added extra Stack navigators for each tab, so they can then in the list, go to the specific screen 
const EventStack = createStackNavigator({
    Events: {
        screen: EventsScreen
    },
    Event:{
      screen:SpecificEventScreenn   
    }
});
const LessonStack = createStackNavigator({
    Lessons: {
        screen: LessonsScreen
    },
    Lesson:{
      screen:SpecificLessonScreen   
    }
});
const PlaceStack = createStackNavigator({
    Places: {
        screen: PlacesScreen
    },
    Place:{
      screen:SpecificPlaceScreen   
    }
});

const AppNavigator = createMaterialTopTabNavigator(
    //Here we add all the buttons and screens where the user can go to on the upper button of challenges 
    {
        Events: {
          screen: EventStack,
          navigationOptions: {
            tabBarIcon: <Image style={{width: 25, height:25}} source={require('../../assets/menubuttons/events.png')}></Image>
          }
        },
        Lessons: {
            screen: LessonStack,
                navigationOptions: {
                    tabBarIcon: <Image style={{width: 25, height:25}} source={require('../../assets/menubuttons/globe.png')}></Image>
                }
            },
        Places: {
            screen: PlaceStack,
            navigationOptions: {
                tabBarIcon: <Image style={{width: 25, height:25}} source={require('../../assets/menubuttons/placemenu.png')}></Image>
            }
          },
    },
    {
        initialRouteName: 'Lessons',  //This code is for the rest of the app to start in the dopamine page   
        tabBarOptions: {
            showIcon: true,
            activeTintColor:'black',  //When a screen is active, his button will show the name under in the navigation tab
            inactiveTintColor: 'white', //otherwise it will not show anything (the background is also white)
            style: {
                marginTop:25,
                borderTopColor: "white", // the bar has a top border by default, seting it to white makes it desapear
                height:60, //Making it a bit bigger
                backgroundColor:"white",
            },
        },
    }
)
export default AppNavigator;