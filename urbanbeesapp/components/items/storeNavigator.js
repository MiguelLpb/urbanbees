import React from 'react';
import { Image } from 'react-native';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import {createStackNavigator} from 'react-navigation-stack';
import StoreScreen from '../screens/StoreScreen';
import SpecificProductScreen from '../screens/SpecificProductScreen';
import MyProductsScreen from '../screens/MyProductsScreen';
import SpecificMyProductsScreen from '../screens/SpecificMyProductScreen';
//added extra Stack navigators for each tab, so they can then in the list, go to the specific screen 
const StoreStack = createStackNavigator({
    Store: {
        screen: StoreScreen
    },
    Product:{
      screen:SpecificProductScreen   
    }
});
const MyProductsStack = createStackNavigator({
    MyProducts: {
        screen: MyProductsScreen
    },
    MyProduct:{
      screen:SpecificMyProductsScreen   
    }
});


const AppNavigator = createMaterialTopTabNavigator(
    //Here we add all the buttons and screens where the user can go to on the upper button of challenges 
    {
        Store: {
          screen: StoreStack,
          navigationOptions: {
            tabBarIcon: <Image style={{width: 25, height:25}} source={require('../../assets/menubuttons/cart.png')}></Image>,

          }
        },
        MyProducts: {
            screen: MyProductsStack,
                navigationOptions: {
                    tabBarIcon: <Image style={{width: 25, height:25}} source={require('../../assets/menubuttons/products.png')}></Image>,
                    title: 'My products'
                }
            },
    },
    {
        initialRouteName: 'Store',  //This code is for the rest of the app to start in the dopamine page   
        tabBarOptions: {
            showIcon: true,
            activeTintColor:'black',  //When a screen is active, his button will show the name under in the navigation tab
            inactiveTintColor: 'white', //otherwise it will not show anything (the background is also white)
            style: {
                marginTop:25,
                borderTopColor: "white", // the bar has a top border by default, seting it to white makes it desapear
                height:60, //Making it a bit bigger
                backgroundColor: 'white',
            },
        },
    }
)
export default AppNavigator;