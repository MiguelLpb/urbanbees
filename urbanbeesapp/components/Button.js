import React, { useState } from 'react';
import { StyleSheet, Text, TouchableOpacity, Image } from 'react-native';

const Button = ({ onPress, colorbutton, children }) => {
    if (colorbutton == "white") {
        return (
            <TouchableOpacity onPress={onPress} style={styles.buttonWhite}>
                <Text style={styles.text}>{children}</Text>
            </TouchableOpacity>
        )
    } else if (colorbutton == "yellow") {
        return (
            <TouchableOpacity onPress={onPress} style={styles.buttonYellow}>
                <Text style={styles.text}>{children}</Text>
            </TouchableOpacity>
        )
    } else if (colorbutton == "orange") {
        return (
            <TouchableOpacity onPress={onPress} style={styles.buttonOrange}>
                <Text style={styles.text}>{children}</Text>
            </TouchableOpacity>
        )
    }else if (colorbutton == "white_small") {
        return (
            <TouchableOpacity onPress={onPress} style={styles.buttonWhiteSmall}>
                <Text style={styles.text}>{children}</Text>
            </TouchableOpacity>
        )
    }else if (colorbutton == "yellow_small") {
        return (
            <TouchableOpacity onPress={onPress} style={styles.buttonYellowSmall}>
                <Text style={styles.text}>{children}</Text>
            </TouchableOpacity>
        )
    } else if (colorbutton == "orange_small") {
        return (
            <TouchableOpacity onPress={onPress} style={styles.buttonOrangeSmall}>
                <Text style={styles.text}>{children}</Text>
            </TouchableOpacity>
        )
    } else if (colorbutton == "light_orange") {
        return (
            <TouchableOpacity onPress={onPress} style={styles.buttonLightOrange}>
                <Text style={styles.text}>{children}</Text>
            </TouchableOpacity>
        )
    } else if (colorbutton == "light_orange_small") {
        return (
            <TouchableOpacity onPress={onPress} style={styles.buttonLightOrangeSmall}>
                <Text style={styles.text}>{children}</Text>
            </TouchableOpacity>
        )
    }
    else {
        return (
            <TouchableOpacity onPress={onPress} style={styles.button}>
                <Text style={styles.text}>{children}</Text>
            </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    buttonWhite: {
        marginTop: 10,
        width: '80%',
        borderRadius:20,
        paddingVertical: 10,
        alignItems: 'center',
        backgroundColor:'white'
    },
    buttonYellow: {
        marginTop: 10,
        width: '80%',
        borderRadius:20,
        paddingVertical: 10,
        alignItems: 'center',
        backgroundColor:'#fffc71'
    },
    buttonOrange: {
        marginTop: 10,
        width: '80%',
        borderRadius:20,
        paddingVertical: 10,
        alignItems: 'center',
        backgroundColor:'#ffb571'
    },
    buttonWhiteSmall: {
        marginTop: 10,
        width: '39%',
        borderRadius:20,
        paddingVertical: 10,
        alignItems: 'center',
        backgroundColor:'white'
    },
    buttonYellowSmall: {
        marginTop: 10,
        width: '39%',
        borderRadius:20,
        paddingVertical: 10,
        alignItems: 'center',
        backgroundColor:'#fffc71'
    },
    buttonOrangeSmall: {
        marginTop: 10,
        width: '39%',
        borderRadius:20,
        paddingVertical: 10,
        alignItems: 'center',
        backgroundColor:'#ffb571'
    },
    buttonOrangeSmall: {
        marginTop: 10,
        width: '39%',
        borderRadius:20,
        paddingVertical: 10,
        alignItems: 'center',
        backgroundColor:'#ffb571'
    },
    buttonLightOrangeSmall: {
        marginTop: 10,
        width: '39%',
        borderRadius:20,
        paddingVertical: 10,
        alignItems: 'center',
        backgroundColor:'#FFA691',
    },
    buttonLightOrange: {
        marginTop: 10,
        width: '80%',
        borderRadius:20,
        paddingVertical: 10,
        alignItems: 'center',
        backgroundColor:'#FFA691',
    },
    button: {
        marginTop: 10,
        padding: 20,
        width: '100%',
        borderRadius: 4,
        alignItems: 'center',
    },
    text: {
        fontWeight: '700',
        fontSize: 18,
        color: "black"
    }
});

export { Button };