import english from '../lenguage/english.json';
import deutsch from '../lenguage/deutsche.json';


 function stringSelector (string){
        let language= global.language=='english'? english:deutsch;
        var textResult='';
        language.map( (data)=>{
            if(data.name==string){
                textResult = data.text;
            }
        });
        return textResult  
  }
  export default stringSelector;
  
  