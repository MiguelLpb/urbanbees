import *  as firebase from 'firebase';
//Created this file with all the setup from firebase ( just copied it from the firebase website in our urbanbees database)
var firebaseConfig = {
  apiKey: "AIzaSyCQ_YZiI9-83fT8vRgd-t5TDyBdx4KMy6c",
  authDomain: "urbanbees-e9017.firebaseapp.com",
  databaseURL: "https://urbanbees-e9017.firebaseio.com",
  projectId: "urbanbees-e9017",
  storageBucket: "urbanbees-e9017.appspot.com",
  messagingSenderId: "678903835124",
  appId: "1:678903835124:web:edb4ac936ab7ca3c22b1a1"
};
var fire=  firebase.initializeApp(firebaseConfig);
export default fire;